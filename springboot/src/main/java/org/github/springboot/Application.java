package org.github.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Application.
 * 
 * @author JYD_XL
 */
@RestController
@RequestMapping("/")
@SpringBootApplication
@EnableTransactionManagement
public class Application extends SpringBootServletInitializer {
    /**
     * main.
     * 
     * @param args String[]
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}