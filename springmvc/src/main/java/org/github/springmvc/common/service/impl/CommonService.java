package org.github.springmvc.common.service.impl;

import org.github.springmvc.common.base.AbstractService;
import org.github.springmvc.common.dao.CommonDao;
import org.github.springmvc.common.service.ICommonService;

import javax.annotation.Resource;
import java.util.Date;

/**
 * CommonService.
 *
 * @author JYD_XL
 */
public abstract class CommonService extends AbstractService implements ICommonService {
    @Resource
    private CommonDao commonDao;

    @Override
    public Date getNow() {
        return commonDao.getNow();
    }
}