package org.github.springmvc.common.constant;

import org.github.spring.javabean.restful.view.VIEWHTMLReturn;

/**
 * HTML page enum.
 *
 * @author JYD_XL
 */
public enum HTMLPageEnum implements VIEWHTMLReturn {
    INDEX("index"), HOME("home"), MAIN("main");

    /** path. */
    private final String _path;

//    /** Resource prefix. */
//    private static final String RESOURCE_PREFIX = "WEB-INF/views/html/";
//
//    /** Resource suffix. */
//    private static final String RESOURCE_SUFFIX = ".html";
//
//    private static final int SIZE = 8192;
//
//    /** Html path-page cache. */
//    private static final Map<String, byte[]> CACHE = new ConcurrentHashMap<>(64);

    /**
     * Constructor.
     *
     * @param path String
     */
    HTMLPageEnum(String path) {
        this._path = path;
    }

    @Override
    public String get() {
        return _path;
    }
//
//    @Override
//    public boolean terminal() {
//        return true;
//    }
//
//    @Override
//    public Resource resource(Supplier<ResourceLoader> loader) {
//        return loader.get().getResource(StringUtil.join(RESOURCE_PREFIX, this.get(), RESOURCE_SUFFIX));
//    }
//
//    @Override
//    public void accept(OutputStream outputStream) throws IOException {
//        outputStream.write(CACHE.get(this.get()));
//    }
//
//    @Override
//    public void collect(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        if (!CACHE.containsKey(this.get())) {loadResource();}
//        this.accept(response.getOutputStream());
//    }
//
//    private void loadResource() throws IOException {
//        try (BufferedInputStream bufferedInputStream = new BufferedInputStream(this.resource(HTMLResourceLoader::new).getInputStream())) {
//            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//            byte[] bytes = new byte[SIZE];
//            int len;
//            while ((len = bufferedInputStream.read(bytes)) != -1) {
//                byteArrayOutputStream.write(bytes, 0, len);
//            }
//            CACHE.put(this.get(), byteArrayOutputStream.toByteArray());
//        }
//    }
}