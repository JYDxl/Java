package org.github.springmvc.common.dao;

import org.github.spring.system.annotation.JdbcTemplateDao;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import java.util.Date;

/**
 * CommonDao.
 *
 * @author JYD_XL
 */
@JdbcTemplateDao
public class CommonDao {
    /** jdbcTemplate. */
    @Resource
    private JdbcTemplate jdbcTemplate;

    /** SQL-now. */
    private String sql = "SELECT NOW()";

    /**
     * getNow.
     *
     * @return Date
     */
    public Date getNow() {
        return jdbcTemplate.queryForObject(sql, null, Date.class);
    }
}