package org.github.springmvc.common.location;

import org.github.spring.javabean.location.PropertyNameLocation;

/**
 * 属性名称枚举.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.lang.Enum
 * @see java.io.Serializable
 * @see java.lang.Comparable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.location.PropertyNameLocation
 */
public enum PropertyNameEnum implements PropertyNameLocation {
    driver("hikariDataSource.driver");

    /** name. */
    private final String _name;

    /**
     * 构造方法.
     *
     * @param name 属性名称
     */
    PropertyNameEnum(String name) {
        this._name = name;
    }

    @Override
    public String get() {
        return _name;
    }
}