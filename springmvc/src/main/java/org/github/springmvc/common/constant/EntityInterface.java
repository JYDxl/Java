package org.github.springmvc.common.constant;

import java.io.Serializable;

/**
 * Entity.
 *
 * @author JYD_XL
 */
public interface EntityInterface extends CodeIndex, Serializable {}