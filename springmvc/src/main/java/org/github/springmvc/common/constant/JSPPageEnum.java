package org.github.springmvc.common.constant;

import org.github.spring.javabean.restful.view.VIEWJSPReturn;

/**
 * JSPPageEnum.
 *
 * @author JYD_XL
 */
public enum JSPPageEnum implements VIEWJSPReturn {
    LOGIN("login");

    /** path. */
    private String _path;

    /**
     * Constructor.
     *
     * @param path String
     */
    private JSPPageEnum(String path) {
        this._path = path;
    }

    @Override
    public String get() {
        return _path;
    }
}