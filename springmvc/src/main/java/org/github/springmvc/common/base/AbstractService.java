package org.github.springmvc.common.base;

import org.github.spring.footstone.AbstractComponent;
import org.github.springmvc.common.constant.CodeIndex;

/**
 * CommonService.
 * 
 * @author JYD_XL
 */
public abstract class AbstractService extends AbstractComponent implements CodeIndex {}