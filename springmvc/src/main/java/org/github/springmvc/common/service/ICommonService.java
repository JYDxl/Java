package org.github.springmvc.common.service;

import java.util.Date;

/**
 * CommonService.
 *
 * @author JYD_XL
 */
public interface ICommonService {
    /**
     * GET DBTime.
     * 
     * @return Date
     */
    Date getNow();
}