package org.github.springmvc.common.constant;

import org.github.spring.system.component.ApplicationContextHolder;
import org.github.spring.system.component.PropsHolder;

/**
 * CodeIndex.
 *
 * @author JYD_XL
 */
public interface CodeIndex {
    /** propsHolder. */
    PropsHolder propsHolder = ApplicationContextHolder.getBean(PropsHolder.class);
}