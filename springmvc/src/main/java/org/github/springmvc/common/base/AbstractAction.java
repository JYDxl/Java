package org.github.springmvc.common.base;

import org.github.spring.footstone.AbstractComponent;
import org.github.springmvc.common.constant.CodeIndex;

/**
 * AbstractAction.
 * 
 * @author JYD_XL
 */
public class AbstractAction extends AbstractComponent implements CodeIndex {}