package org.github.springmvc.module.index.action;

import org.github.spring.javabean.restful.VIEWReturn;
import org.github.spring.system.annotation.WebController;
import org.github.springmvc.common.base.AbstractAction;
import org.github.springmvc.common.constant.HTMLPageEnum;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author JYD_XL
 */
@WebController
@RequestMapping("/")
public class IndexAction extends AbstractAction {
    /**
     * index.
     * 
     * @return VIEWReturn
     */
    @RequestMapping(method = RequestMethod.GET)
    public VIEWReturn index() {
        return HTMLPageEnum.INDEX;
    }
}