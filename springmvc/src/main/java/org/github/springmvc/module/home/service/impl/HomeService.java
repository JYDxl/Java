package org.github.springmvc.module.home.service.impl;

import lombok.val;
import org.github.spring.footstone.AbstractCondModel;
import org.github.spring.javabean.restful.JSONReturn;
import org.github.spring.javabean.restful.json.JSONPReturn;
import org.github.spring.javabean.restful.json.JSONPageReturn;
import org.github.spring.support.CrudStarter;
import org.github.springmvc.auto.entity.UsersEntity;
import org.github.springmvc.auto.entity.UsersExample;
import org.github.springmvc.auto.mapper.UsersMapper;
import org.github.springmvc.common.service.impl.CommonService;
import org.github.springmvc.module.home.model.TeacherRestModel;
import org.github.springmvc.module.home.service.IHomeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * HomeServiceImpl.
 *
 * @author JYD_XL
 */
@Service
public class HomeService extends CommonService implements IHomeService {
    @Resource
    private UsersMapper usersMapper;

    @Override
    public JSONReturn getTime(String callback) {
        return new JSONPReturn<>(callback, this.getNow());
    }

    @Override
    public JSONReturn search(AbstractCondModel condModel) {
        UsersExample example = new UsersExample();
        UsersExample.Criteria criteria = example.createCriteria();
        CrudStarter.startCrud(condModel, criteria);
        val page = usersMapper.selectByExampleWithRowbounds(example, condModel.getRowBounds());
        val data = page.stream().map(TeacherRestModel::new).toArray(UsersEntity[]::new);
        return new JSONPageReturn<>(page, data);
    }
}