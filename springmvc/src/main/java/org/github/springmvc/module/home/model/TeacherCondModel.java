package org.github.springmvc.module.home.model;

import lombok.*;
import org.github.spring.footstone.AbstractCondModel;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * TeacherCondModel.
 * Bean and Method as unit.
 *
 * @author JYD_XL
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TeacherCondModel extends AbstractCondModel {
    /** number. */
    @NumberFormat(pattern = "#.##")
    private BigDecimal number;

    /** number. */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date datetime;
}