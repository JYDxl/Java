package org.github.springmvc.module.home.service;

import org.github.spring.footstone.AbstractCondModel;
import org.github.spring.javabean.restful.JSONReturn;
import org.github.springmvc.common.service.ICommonService;

/**
 * CommonService.
 *
 * @author JYD_XL
 */
public interface IHomeService extends ICommonService {
    /** search. */
    JSONReturn search(AbstractCondModel condModel);

    /**
     * GET time.
     *
     * @param callback String
     * @return Date
     */
    JSONReturn getTime(String callback);
}