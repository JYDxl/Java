package org.github.springmvc.module.home.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import org.github.springmvc.auto.entity.UsersEntity;
import org.github.springmvc.common.constant.EntityInterface;
import org.springframework.beans.BeanUtils;

/**
 * TeacherRestModel.
 *
 * @author JYD_XL
 */
@SuppressWarnings("serial")
public class TeacherRestModel extends UsersEntity implements EntityInterface {
    /** Constructor. */
    public TeacherRestModel() {}

    /**
     * Constructor.
     *
     * @param entity Teacher
     */
    public TeacherRestModel(UsersEntity entity) {
        BeanUtils.copyProperties(entity, this);
    }

    @JsonFormat(pattern = "HH:mm:ss")
    public Date getDate() {
        return new Date();
    }

    public String getNull() {
        return null;
    }

    public Date getTime() {
        return new Date();
    }
}