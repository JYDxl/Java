package org.github.springmvc.module.home.action;

import com.fasterxml.jackson.databind.JsonNode;
import org.github.spring.javabean.annotation.Invoke;
import org.github.spring.javabean.restful.Returnable;
import org.github.spring.javabean.restful.json.JSONDataReturn;
import org.github.spring.system.annotation.WebController;
import org.github.springmvc.common.base.AbstractAction;
import org.github.springmvc.common.location.PropertyNameEnum;
import org.github.springmvc.module.home.model.TeacherCondModel;
import org.github.springmvc.module.home.service.IHomeService;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Map;

import static org.github.springmvc.common.constant.HTMLPageEnum.HOME;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * HomeAction.
 *
 * @author JYD_XL
 */
@WebController
@RequestMapping("/home/")
public class HomeAction extends AbstractAction {
    @Resource
    private IHomeService homeService;

    /**
     * home.
     *
     * @return VIEWReturn
     */
    @RequestMapping(method = GET)
    public Returnable home() {
        return HOME;
    }

    /**
     * search.
     *
     * @return JSONReturn
     */
    @RequestMapping(path = "ssm", method = GET)
    public Returnable search(TeacherCondModel condModel) {
        return homeService.search(condModel);
    }

    /**
     * format.
     *
     * @param condModel TeacherCondModel
     * @return JSONReturn
     */
    @RequestMapping(path = "format", method = POST)
    public Returnable format(TeacherCondModel condModel) {
        return condModel.getDatetime().toString().concat("\t").concat(condModel.getNumber().toString())::toString;
    }

    /**
     * callback.
     *
     * @param callback String
     * @return JSONReturn
     */
    @RequestMapping(path = "bilibili", method = GET)
    public Returnable callback(String callback) {
        return homeService.getTime(callback);
    }

    /**
     * lambda.
     *
     * @return JSONReturn
     */
    @RequestMapping(path = "lambda", method = POST)
    public Returnable lambda(@Invoke Map<String, String> node) {
        return new JSONDataReturn<>(propsHolder.apply(PropertyNameEnum.driver));
    }

    /**
     * holder.
     *
     * @return JSONReturn
     */
    @RequestMapping(path = "holder", method = GET)
    public Returnable holder(@Invoke("bilibili") JsonNode node) {
        return new JSONDataReturn<>(propsHolder.apply(PropertyNameEnum.driver));
    }
}