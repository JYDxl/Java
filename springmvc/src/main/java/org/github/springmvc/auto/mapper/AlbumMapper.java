/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.AlbumEntity;
import org.github.springmvc.auto.entity.AlbumExample;
import org.github.springmvc.auto.entity.AlbumKey;

@QueryInterface
public interface AlbumMapper extends Interface {
    long countByExample(AlbumExample example);

    int deleteByExample(AlbumExample example);

    int deleteByPrimaryKey(AlbumKey key);

    int insert(AlbumEntity record);

    int insertSelective(AlbumEntity record);

    List<AlbumEntity> selectByExampleWithRowbounds(AlbumExample example, RowBounds rowBounds);

    List<AlbumEntity> selectByExample(AlbumExample example);

    AlbumEntity selectByPrimaryKey(AlbumKey key);

    int updateByExampleSelective(@Param("record") AlbumEntity record, @Param("example") AlbumExample example);

    int updateByExample(@Param("record") AlbumEntity record, @Param("example") AlbumExample example);

    int updateByPrimaryKeySelective(AlbumEntity record);

    int updateByPrimaryKey(AlbumEntity record);

    int batchUpdate(List<AlbumEntity> list);

    int batchInsert(List<AlbumEntity> list);

    int upsert(@Param("record") AlbumEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") AlbumEntity record, @Param("array") String[] array);
}