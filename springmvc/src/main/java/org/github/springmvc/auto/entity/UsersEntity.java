/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import java.util.Date;

/**
 * UsersEntity [ t_users ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class UsersEntity extends UsersKey implements Serializable {
    /** 用户名(t_users.username). */
    private String username;

    /** 用户密码(t_users.password). */
    private String password;

    /** 密保问题(t_users.question). */
    private String question;

    /** 密保答案(t_users.answer). */
    private String answer;

    /** 用户图片(t_users.user_img). */
    private String userImg;

    /** 注册时间(t_users.regtime). */
    private Date regtime;

    private static final long serialVersionUID = 1L;

    public UsersEntity(String userId, String username, String password, String question, String answer, String userImg, Date regtime) {
        super(userId);
        this.username = username;
        this.password = password;
        this.question = question;
        this.answer = answer;
        this.userImg = userImg;
        this.regtime = regtime;
    }

    public UsersEntity() {
        super();
    }

    /**
     * GET 用户名(t_users.username).
     *
     * @return username VARCHAR
     */
    public String getUsername() {
        return username;
    }

    public UsersEntity withUsername(String username) {
        this.setUsername(username);
        return this;
    }

    /**
     * SET 用户名(t_users.username).
     *
     * @param username VARCHAR
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * GET 用户密码(t_users.password).
     *
     * @return password VARCHAR
     */
    public String getPassword() {
        return password;
    }

    public UsersEntity withPassword(String password) {
        this.setPassword(password);
        return this;
    }

    /**
     * SET 用户密码(t_users.password).
     *
     * @param password VARCHAR
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * GET 密保问题(t_users.question).
     *
     * @return question VARCHAR
     */
    public String getQuestion() {
        return question;
    }

    public UsersEntity withQuestion(String question) {
        this.setQuestion(question);
        return this;
    }

    /**
     * SET 密保问题(t_users.question).
     *
     * @param question VARCHAR
     */
    public void setQuestion(String question) {
        this.question = question == null ? null : question.trim();
    }

    /**
     * GET 密保答案(t_users.answer).
     *
     * @return answer VARCHAR
     */
    public String getAnswer() {
        return answer;
    }

    public UsersEntity withAnswer(String answer) {
        this.setAnswer(answer);
        return this;
    }

    /**
     * SET 密保答案(t_users.answer).
     *
     * @param answer VARCHAR
     */
    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }

    /**
     * GET 用户图片(t_users.user_img).
     *
     * @return userImg VARCHAR
     */
    public String getUserImg() {
        return userImg;
    }

    public UsersEntity withUserImg(String userImg) {
        this.setUserImg(userImg);
        return this;
    }

    /**
     * SET 用户图片(t_users.user_img).
     *
     * @param userImg VARCHAR
     */
    public void setUserImg(String userImg) {
        this.userImg = userImg == null ? null : userImg.trim();
    }

    /**
     * GET 注册时间(t_users.regtime).
     *
     * @return regtime TIMESTAMP
     */
    public Date getRegtime() {
        return regtime;
    }

    public UsersEntity withRegtime(Date regtime) {
        this.setRegtime(regtime);
        return this;
    }

    /**
     * SET 注册时间(t_users.regtime).
     *
     * @param regtime TIMESTAMP
     */
    public void setRegtime(Date regtime) {
        this.regtime = regtime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", question=").append(question);
        sb.append(", answer=").append(answer);
        sb.append(", userImg=").append(userImg);
        sb.append(", regtime=").append(regtime);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UsersEntity other = (UsersEntity) that;
        return (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getUsername() == null ? other.getUsername() == null : this.getUsername().equals(other.getUsername()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getQuestion() == null ? other.getQuestion() == null : this.getQuestion().equals(other.getQuestion()))
            && (this.getAnswer() == null ? other.getAnswer() == null : this.getAnswer().equals(other.getAnswer()))
            && (this.getUserImg() == null ? other.getUserImg() == null : this.getUserImg().equals(other.getUserImg()))
            && (this.getRegtime() == null ? other.getRegtime() == null : this.getRegtime().equals(other.getRegtime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getQuestion() == null) ? 0 : getQuestion().hashCode());
        result = prime * result + ((getAnswer() == null) ? 0 : getAnswer().hashCode());
        result = prime * result + ((getUserImg() == null) ? 0 : getUserImg().hashCode());
        result = prime * result + ((getRegtime() == null) ? 0 : getRegtime().hashCode());
        return result;
    }
}