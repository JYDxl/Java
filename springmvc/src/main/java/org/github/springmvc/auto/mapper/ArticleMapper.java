/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.ArticleEntity;
import org.github.springmvc.auto.entity.ArticleExample;
import org.github.springmvc.auto.entity.ArticleKey;

@QueryInterface
public interface ArticleMapper extends Interface {
    long countByExample(ArticleExample example);

    int deleteByExample(ArticleExample example);

    int deleteByPrimaryKey(ArticleKey key);

    int insert(ArticleEntity record);

    int insertSelective(ArticleEntity record);

    List<ArticleEntity> selectByExampleWithRowbounds(ArticleExample example, RowBounds rowBounds);

    List<ArticleEntity> selectByExample(ArticleExample example);

    ArticleEntity selectByPrimaryKey(ArticleKey key);

    int updateByExampleSelective(@Param("record") ArticleEntity record, @Param("example") ArticleExample example);

    int updateByExample(@Param("record") ArticleEntity record, @Param("example") ArticleExample example);

    int updateByPrimaryKeySelective(ArticleEntity record);

    int updateByPrimaryKey(ArticleEntity record);

    int batchUpdate(List<ArticleEntity> list);

    int batchInsert(List<ArticleEntity> list);

    int upsert(@Param("record") ArticleEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") ArticleEntity record, @Param("array") String[] array);
}