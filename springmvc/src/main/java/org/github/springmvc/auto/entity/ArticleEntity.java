/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;

/**
 * ArticleEntity [ article ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class ArticleEntity extends ArticleKey implements Serializable {
    /** title(article.title). */
    private String title;

    /** content(article.content). */
    private String content;

    /** image(article.image). */
    private String image;

    /** time(article.time). */
    private String time;

    /** editer(article.editer). */
    private String editer;

    /** count(article.count). */
    private Integer count;

    /** count1(article.count1). */
    private Integer count1;

    /** type(article.type). */
    private String type;

    /** notice(article.notice). */
    private String notice;

    /** keywords(article.keywords). */
    private String keywords;

    private static final long serialVersionUID = 1L;

    public ArticleEntity(Integer id, String title, String content, String image, String time, String editer, Integer count, Integer count1, String type, String notice, String keywords) {
        super(id);
        this.title = title;
        this.content = content;
        this.image = image;
        this.time = time;
        this.editer = editer;
        this.count = count;
        this.count1 = count1;
        this.type = type;
        this.notice = notice;
        this.keywords = keywords;
    }

    public ArticleEntity() {
        super();
    }

    /**
     * GET title(article.title).
     *
     * @return title VARCHAR
     */
    public String getTitle() {
        return title;
    }

    public ArticleEntity withTitle(String title) {
        this.setTitle(title);
        return this;
    }

    /**
     * SET title(article.title).
     *
     * @param title VARCHAR
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * GET content(article.content).
     *
     * @return content VARCHAR
     */
    public String getContent() {
        return content;
    }

    public ArticleEntity withContent(String content) {
        this.setContent(content);
        return this;
    }

    /**
     * SET content(article.content).
     *
     * @param content VARCHAR
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * GET image(article.image).
     *
     * @return image VARCHAR
     */
    public String getImage() {
        return image;
    }

    public ArticleEntity withImage(String image) {
        this.setImage(image);
        return this;
    }

    /**
     * SET image(article.image).
     *
     * @param image VARCHAR
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * GET time(article.time).
     *
     * @return time VARCHAR
     */
    public String getTime() {
        return time;
    }

    public ArticleEntity withTime(String time) {
        this.setTime(time);
        return this;
    }

    /**
     * SET time(article.time).
     *
     * @param time VARCHAR
     */
    public void setTime(String time) {
        this.time = time == null ? null : time.trim();
    }

    /**
     * GET editer(article.editer).
     *
     * @return editer VARCHAR
     */
    public String getEditer() {
        return editer;
    }

    public ArticleEntity withEditer(String editer) {
        this.setEditer(editer);
        return this;
    }

    /**
     * SET editer(article.editer).
     *
     * @param editer VARCHAR
     */
    public void setEditer(String editer) {
        this.editer = editer == null ? null : editer.trim();
    }

    /**
     * GET count(article.count).
     *
     * @return count INTEGER
     */
    public Integer getCount() {
        return count;
    }

    public ArticleEntity withCount(Integer count) {
        this.setCount(count);
        return this;
    }

    /**
     * SET count(article.count).
     *
     * @param count INTEGER
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * GET count1(article.count1).
     *
     * @return count1 INTEGER
     */
    public Integer getCount1() {
        return count1;
    }

    public ArticleEntity withCount1(Integer count1) {
        this.setCount1(count1);
        return this;
    }

    /**
     * SET count1(article.count1).
     *
     * @param count1 INTEGER
     */
    public void setCount1(Integer count1) {
        this.count1 = count1;
    }

    /**
     * GET type(article.type).
     *
     * @return type VARCHAR
     */
    public String getType() {
        return type;
    }

    public ArticleEntity withType(String type) {
        this.setType(type);
        return this;
    }

    /**
     * SET type(article.type).
     *
     * @param type VARCHAR
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * GET notice(article.notice).
     *
     * @return notice VARCHAR
     */
    public String getNotice() {
        return notice;
    }

    public ArticleEntity withNotice(String notice) {
        this.setNotice(notice);
        return this;
    }

    /**
     * SET notice(article.notice).
     *
     * @param notice VARCHAR
     */
    public void setNotice(String notice) {
        this.notice = notice == null ? null : notice.trim();
    }

    /**
     * GET keywords(article.keywords).
     *
     * @return keywords VARCHAR
     */
    public String getKeywords() {
        return keywords;
    }

    public ArticleEntity withKeywords(String keywords) {
        this.setKeywords(keywords);
        return this;
    }

    /**
     * SET keywords(article.keywords).
     *
     * @param keywords VARCHAR
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords == null ? null : keywords.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", title=").append(title);
        sb.append(", content=").append(content);
        sb.append(", image=").append(image);
        sb.append(", time=").append(time);
        sb.append(", editer=").append(editer);
        sb.append(", count=").append(count);
        sb.append(", count1=").append(count1);
        sb.append(", type=").append(type);
        sb.append(", notice=").append(notice);
        sb.append(", keywords=").append(keywords);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ArticleEntity other = (ArticleEntity) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getImage() == null ? other.getImage() == null : this.getImage().equals(other.getImage()))
            && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
            && (this.getEditer() == null ? other.getEditer() == null : this.getEditer().equals(other.getEditer()))
            && (this.getCount() == null ? other.getCount() == null : this.getCount().equals(other.getCount()))
            && (this.getCount1() == null ? other.getCount1() == null : this.getCount1().equals(other.getCount1()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getNotice() == null ? other.getNotice() == null : this.getNotice().equals(other.getNotice()))
            && (this.getKeywords() == null ? other.getKeywords() == null : this.getKeywords().equals(other.getKeywords()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getImage() == null) ? 0 : getImage().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((getEditer() == null) ? 0 : getEditer().hashCode());
        result = prime * result + ((getCount() == null) ? 0 : getCount().hashCode());
        result = prime * result + ((getCount1() == null) ? 0 : getCount1().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getNotice() == null) ? 0 : getNotice().hashCode());
        result = prime * result + ((getKeywords() == null) ? 0 : getKeywords().hashCode());
        return result;
    }
}