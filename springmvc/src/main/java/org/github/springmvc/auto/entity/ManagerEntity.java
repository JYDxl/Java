/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;

/**
 * ManagerEntity [ manager ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class ManagerEntity extends ManagerKey implements Serializable {
    /** account(manager.account). */
    private String account;

    /** password(manager.password). */
    private String password;

    /** role(manager.role). */
    private String role;

    /** notice(manager.notice). */
    private String notice;

    private static final long serialVersionUID = 1L;

    public ManagerEntity(Integer id, String account, String password, String role, String notice) {
        super(id);
        this.account = account;
        this.password = password;
        this.role = role;
        this.notice = notice;
    }

    public ManagerEntity() {
        super();
    }

    /**
     * GET account(manager.account).
     *
     * @return account VARCHAR
     */
    public String getAccount() {
        return account;
    }

    public ManagerEntity withAccount(String account) {
        this.setAccount(account);
        return this;
    }

    /**
     * SET account(manager.account).
     *
     * @param account VARCHAR
     */
    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    /**
     * GET password(manager.password).
     *
     * @return password VARCHAR
     */
    public String getPassword() {
        return password;
    }

    public ManagerEntity withPassword(String password) {
        this.setPassword(password);
        return this;
    }

    /**
     * SET password(manager.password).
     *
     * @param password VARCHAR
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * GET role(manager.role).
     *
     * @return role VARCHAR
     */
    public String getRole() {
        return role;
    }

    public ManagerEntity withRole(String role) {
        this.setRole(role);
        return this;
    }

    /**
     * SET role(manager.role).
     *
     * @param role VARCHAR
     */
    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    /**
     * GET notice(manager.notice).
     *
     * @return notice VARCHAR
     */
    public String getNotice() {
        return notice;
    }

    public ManagerEntity withNotice(String notice) {
        this.setNotice(notice);
        return this;
    }

    /**
     * SET notice(manager.notice).
     *
     * @param notice VARCHAR
     */
    public void setNotice(String notice) {
        this.notice = notice == null ? null : notice.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", account=").append(account);
        sb.append(", password=").append(password);
        sb.append(", role=").append(role);
        sb.append(", notice=").append(notice);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ManagerEntity other = (ManagerEntity) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getAccount() == null ? other.getAccount() == null : this.getAccount().equals(other.getAccount()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getRole() == null ? other.getRole() == null : this.getRole().equals(other.getRole()))
            && (this.getNotice() == null ? other.getNotice() == null : this.getNotice().equals(other.getNotice()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getAccount() == null) ? 0 : getAccount().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getRole() == null) ? 0 : getRole().hashCode());
        result = prime * result + ((getNotice() == null) ? 0 : getNotice().hashCode());
        return result;
    }
}