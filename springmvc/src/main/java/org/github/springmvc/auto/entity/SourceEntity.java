/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import java.util.Date;

/**
 * SourceEntity [ t_source ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class SourceEntity extends SourceKey implements Serializable {
    /** 用户ID（外键）(t_source.user_id). */
    private String userId;

    /** 标题(t_source.title). */
    private String title;

    /** 类型(t_source.type). */
    private String type;

    /** 描述信息(t_source.sdecotation). */
    private String sdecotation;

    /** 资源下载地址(t_source.source_url). */
    private String sourceUrl;

    /** 创建时间(t_source.create_time). */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public SourceEntity(String sourceId, String userId, String title, String type, String sdecotation, String sourceUrl, Date createTime) {
        super(sourceId);
        this.userId = userId;
        this.title = title;
        this.type = type;
        this.sdecotation = sdecotation;
        this.sourceUrl = sourceUrl;
        this.createTime = createTime;
    }

    public SourceEntity() {
        super();
    }

    /**
     * GET 用户ID（外键）(t_source.user_id).
     *
     * @return userId VARCHAR
     */
    public String getUserId() {
        return userId;
    }

    public SourceEntity withUserId(String userId) {
        this.setUserId(userId);
        return this;
    }

    /**
     * SET 用户ID（外键）(t_source.user_id).
     *
     * @param userId VARCHAR
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * GET 标题(t_source.title).
     *
     * @return title VARCHAR
     */
    public String getTitle() {
        return title;
    }

    public SourceEntity withTitle(String title) {
        this.setTitle(title);
        return this;
    }

    /**
     * SET 标题(t_source.title).
     *
     * @param title VARCHAR
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * GET 类型(t_source.type).
     *
     * @return type VARCHAR
     */
    public String getType() {
        return type;
    }

    public SourceEntity withType(String type) {
        this.setType(type);
        return this;
    }

    /**
     * SET 类型(t_source.type).
     *
     * @param type VARCHAR
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * GET 描述信息(t_source.sdecotation).
     *
     * @return sdecotation VARCHAR
     */
    public String getSdecotation() {
        return sdecotation;
    }

    public SourceEntity withSdecotation(String sdecotation) {
        this.setSdecotation(sdecotation);
        return this;
    }

    /**
     * SET 描述信息(t_source.sdecotation).
     *
     * @param sdecotation VARCHAR
     */
    public void setSdecotation(String sdecotation) {
        this.sdecotation = sdecotation == null ? null : sdecotation.trim();
    }

    /**
     * GET 资源下载地址(t_source.source_url).
     *
     * @return sourceUrl VARCHAR
     */
    public String getSourceUrl() {
        return sourceUrl;
    }

    public SourceEntity withSourceUrl(String sourceUrl) {
        this.setSourceUrl(sourceUrl);
        return this;
    }

    /**
     * SET 资源下载地址(t_source.source_url).
     *
     * @param sourceUrl VARCHAR
     */
    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl == null ? null : sourceUrl.trim();
    }

    /**
     * GET 创建时间(t_source.create_time).
     *
     * @return createTime TIMESTAMP
     */
    public Date getCreateTime() {
        return createTime;
    }

    public SourceEntity withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    /**
     * SET 创建时间(t_source.create_time).
     *
     * @param createTime TIMESTAMP
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userId=").append(userId);
        sb.append(", title=").append(title);
        sb.append(", type=").append(type);
        sb.append(", sdecotation=").append(sdecotation);
        sb.append(", sourceUrl=").append(sourceUrl);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SourceEntity other = (SourceEntity) that;
        return (this.getSourceId() == null ? other.getSourceId() == null : this.getSourceId().equals(other.getSourceId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getSdecotation() == null ? other.getSdecotation() == null : this.getSdecotation().equals(other.getSdecotation()))
            && (this.getSourceUrl() == null ? other.getSourceUrl() == null : this.getSourceUrl().equals(other.getSourceUrl()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getSourceId() == null) ? 0 : getSourceId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getSdecotation() == null) ? 0 : getSdecotation().hashCode());
        result = prime * result + ((getSourceUrl() == null) ? 0 : getSourceUrl().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        return result;
    }
}