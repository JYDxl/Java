/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;

/**
 * CritiqueEntity [ critique ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class CritiqueEntity extends CritiqueKey implements Serializable {
    /** article_id(critique.article_id). */
    private Integer articleId;

    /** content(critique.content). */
    private String content;

    /** name(critique.name). */
    private String name;

    /** time(critique.time). */
    private String time;

    /** photo(critique.photo). */
    private String photo;

    /** type(critique.type). */
    private String type;

    /** notice(critique.notice). */
    private String notice;

    private static final long serialVersionUID = 1L;

    public CritiqueEntity(Integer id, Integer articleId, String content, String name, String time, String photo, String type, String notice) {
        super(id);
        this.articleId = articleId;
        this.content = content;
        this.name = name;
        this.time = time;
        this.photo = photo;
        this.type = type;
        this.notice = notice;
    }

    public CritiqueEntity() {
        super();
    }

    /**
     * GET article_id(critique.article_id).
     *
     * @return articleId INTEGER
     */
    public Integer getArticleId() {
        return articleId;
    }

    public CritiqueEntity withArticleId(Integer articleId) {
        this.setArticleId(articleId);
        return this;
    }

    /**
     * SET article_id(critique.article_id).
     *
     * @param articleId INTEGER
     */
    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    /**
     * GET content(critique.content).
     *
     * @return content VARCHAR
     */
    public String getContent() {
        return content;
    }

    public CritiqueEntity withContent(String content) {
        this.setContent(content);
        return this;
    }

    /**
     * SET content(critique.content).
     *
     * @param content VARCHAR
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * GET name(critique.name).
     *
     * @return name VARCHAR
     */
    public String getName() {
        return name;
    }

    public CritiqueEntity withName(String name) {
        this.setName(name);
        return this;
    }

    /**
     * SET name(critique.name).
     *
     * @param name VARCHAR
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * GET time(critique.time).
     *
     * @return time VARCHAR
     */
    public String getTime() {
        return time;
    }

    public CritiqueEntity withTime(String time) {
        this.setTime(time);
        return this;
    }

    /**
     * SET time(critique.time).
     *
     * @param time VARCHAR
     */
    public void setTime(String time) {
        this.time = time == null ? null : time.trim();
    }

    /**
     * GET photo(critique.photo).
     *
     * @return photo VARCHAR
     */
    public String getPhoto() {
        return photo;
    }

    public CritiqueEntity withPhoto(String photo) {
        this.setPhoto(photo);
        return this;
    }

    /**
     * SET photo(critique.photo).
     *
     * @param photo VARCHAR
     */
    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    /**
     * GET type(critique.type).
     *
     * @return type VARCHAR
     */
    public String getType() {
        return type;
    }

    public CritiqueEntity withType(String type) {
        this.setType(type);
        return this;
    }

    /**
     * SET type(critique.type).
     *
     * @param type VARCHAR
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * GET notice(critique.notice).
     *
     * @return notice VARCHAR
     */
    public String getNotice() {
        return notice;
    }

    public CritiqueEntity withNotice(String notice) {
        this.setNotice(notice);
        return this;
    }

    /**
     * SET notice(critique.notice).
     *
     * @param notice VARCHAR
     */
    public void setNotice(String notice) {
        this.notice = notice == null ? null : notice.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", articleId=").append(articleId);
        sb.append(", content=").append(content);
        sb.append(", name=").append(name);
        sb.append(", time=").append(time);
        sb.append(", photo=").append(photo);
        sb.append(", type=").append(type);
        sb.append(", notice=").append(notice);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CritiqueEntity other = (CritiqueEntity) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getArticleId() == null ? other.getArticleId() == null : this.getArticleId().equals(other.getArticleId()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
            && (this.getPhoto() == null ? other.getPhoto() == null : this.getPhoto().equals(other.getPhoto()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getNotice() == null ? other.getNotice() == null : this.getNotice().equals(other.getNotice()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getArticleId() == null) ? 0 : getArticleId().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((getPhoto() == null) ? 0 : getPhoto().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getNotice() == null) ? 0 : getNotice().hashCode());
        return result;
    }
}