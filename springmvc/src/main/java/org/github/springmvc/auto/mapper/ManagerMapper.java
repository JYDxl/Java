/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.ManagerEntity;
import org.github.springmvc.auto.entity.ManagerExample;
import org.github.springmvc.auto.entity.ManagerKey;

@QueryInterface
public interface ManagerMapper extends Interface {
    long countByExample(ManagerExample example);

    int deleteByExample(ManagerExample example);

    int deleteByPrimaryKey(ManagerKey key);

    int insert(ManagerEntity record);

    int insertSelective(ManagerEntity record);

    List<ManagerEntity> selectByExampleWithRowbounds(ManagerExample example, RowBounds rowBounds);

    List<ManagerEntity> selectByExample(ManagerExample example);

    ManagerEntity selectByPrimaryKey(ManagerKey key);

    int updateByExampleSelective(@Param("record") ManagerEntity record, @Param("example") ManagerExample example);

    int updateByExample(@Param("record") ManagerEntity record, @Param("example") ManagerExample example);

    int updateByPrimaryKeySelective(ManagerEntity record);

    int updateByPrimaryKey(ManagerEntity record);

    int batchUpdate(List<ManagerEntity> list);

    int batchInsert(List<ManagerEntity> list);

    int upsert(@Param("record") ManagerEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") ManagerEntity record, @Param("array") String[] array);
}