/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;

/**
 * AlbumEntity [ album ].
 *
 * @author JYD_XL
 * @version 2017-05-25
 */
@QueryEntity
public class AlbumEntity extends AlbumKey implements Serializable {
    /** title(album.title). */
    private String title;

    /** image(album.image). */
    private String image;

    /** content(album.content). */
    private String content;

    /** notice(album.notice). */
    private String notice;

    /** time(album.time). */
    private String time;

    private static final long serialVersionUID = 1L;

    public AlbumEntity(Integer id, String title, String image, String content, String notice, String time) {
        super(id);
        this.title = title;
        this.image = image;
        this.content = content;
        this.notice = notice;
        this.time = time;
    }

    public AlbumEntity() {
        super();
    }

    /**
     * GET title(album.title).
     *
     * @return title VARCHAR
     */
    public String getTitle() {
        return title;
    }

    public AlbumEntity withTitle(String title) {
        this.setTitle(title);
        return this;
    }

    /**
     * SET title(album.title).
     *
     * @param title VARCHAR
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * GET image(album.image).
     *
     * @return image VARCHAR
     */
    public String getImage() {
        return image;
    }

    public AlbumEntity withImage(String image) {
        this.setImage(image);
        return this;
    }

    /**
     * SET image(album.image).
     *
     * @param image VARCHAR
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * GET content(album.content).
     *
     * @return content VARCHAR
     */
    public String getContent() {
        return content;
    }

    public AlbumEntity withContent(String content) {
        this.setContent(content);
        return this;
    }

    /**
     * SET content(album.content).
     *
     * @param content VARCHAR
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * GET notice(album.notice).
     *
     * @return notice VARCHAR
     */
    public String getNotice() {
        return notice;
    }

    public AlbumEntity withNotice(String notice) {
        this.setNotice(notice);
        return this;
    }

    /**
     * SET notice(album.notice).
     *
     * @param notice VARCHAR
     */
    public void setNotice(String notice) {
        this.notice = notice == null ? null : notice.trim();
    }

    /**
     * GET time(album.time).
     *
     * @return time VARCHAR
     */
    public String getTime() {
        return time;
    }

    public AlbumEntity withTime(String time) {
        this.setTime(time);
        return this;
    }

    /**
     * SET time(album.time).
     *
     * @param time VARCHAR
     */
    public void setTime(String time) {
        this.time = time == null ? null : time.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", title=").append(title);
        sb.append(", image=").append(image);
        sb.append(", content=").append(content);
        sb.append(", notice=").append(notice);
        sb.append(", time=").append(time);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        AlbumEntity other = (AlbumEntity) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
                && (this.getImage() == null ? other.getImage() == null : this.getImage().equals(other.getImage()))
                && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
                && (this.getNotice() == null ? other.getNotice() == null : this.getNotice().equals(other.getNotice()))
                && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getImage() == null) ? 0 : getImage().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getNotice() == null) ? 0 : getNotice().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        return result;
    }
}