/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import org.github.spring.footstone.Entity;

/**
 * ResponseOneKey [ t_response_one ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class ResponseOneKey extends Entity implements Serializable {
    /** 回复ID（主键）(t_response_one.post_id). */
    private String postId;

    private static final long serialVersionUID = 1L;

    public ResponseOneKey(String postId) {
        this.postId = postId;
    }

    public ResponseOneKey() {
        super();
    }

    /**
     * GET 回复ID（主键）(t_response_one.post_id).
     *
     * @return postId VARCHAR
     */
    public String getPostId() {
        return postId;
    }

    public ResponseOneKey withPostId(String postId) {
        this.setPostId(postId);
        return this;
    }

    /**
     * SET 回复ID（主键）(t_response_one.post_id).
     *
     * @param postId VARCHAR
     */
    public void setPostId(String postId) {
        this.postId = postId == null ? null : postId.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", postId=").append(postId);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ResponseOneKey other = (ResponseOneKey) that;
        return (this.getPostId() == null ? other.getPostId() == null : this.getPostId().equals(other.getPostId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPostId() == null) ? 0 : getPostId().hashCode());
        return result;
    }
}