/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import org.github.spring.footstone.Entity;

/**
 * UsersInfoKey [ t_users_info ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class UsersInfoKey extends Entity implements Serializable {
    /** 用户ID（主键+外键）(t_users_info.user_id). */
    private String userId;

    private static final long serialVersionUID = 1L;

    public UsersInfoKey(String userId) {
        this.userId = userId;
    }

    public UsersInfoKey() {
        super();
    }

    /**
     * GET 用户ID（主键+外键）(t_users_info.user_id).
     *
     * @return userId VARCHAR
     */
    public String getUserId() {
        return userId;
    }

    public UsersInfoKey withUserId(String userId) {
        this.setUserId(userId);
        return this;
    }

    /**
     * SET 用户ID（主键+外键）(t_users_info.user_id).
     *
     * @param userId VARCHAR
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userId=").append(userId);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UsersInfoKey other = (UsersInfoKey) that;
        return (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        return result;
    }
}