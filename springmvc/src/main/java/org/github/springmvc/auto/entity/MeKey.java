/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import org.github.spring.footstone.Entity;

/**
 * MeKey [ me ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class MeKey extends Entity implements Serializable {
    /** id(me.id). */
    private Integer id;

    private static final long serialVersionUID = 1L;

    public MeKey(Integer id) {
        this.id = id;
    }

    public MeKey() {
        super();
    }

    /**
     * GET id(me.id).
     *
     * @return id INTEGER
     */
    public Integer getId() {
        return id;
    }

    public MeKey withId(Integer id) {
        this.setId(id);
        return this;
    }

    /**
     * SET id(me.id).
     *
     * @param id INTEGER
     */
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        MeKey other = (MeKey) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }
}