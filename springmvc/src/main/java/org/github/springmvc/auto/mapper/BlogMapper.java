/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.BlogEntity;
import org.github.springmvc.auto.entity.BlogExample;
import org.github.springmvc.auto.entity.BlogKey;

@QueryInterface
public interface BlogMapper extends Interface {
    long countByExample(BlogExample example);

    int deleteByExample(BlogExample example);

    int deleteByPrimaryKey(BlogKey key);

    int insert(BlogEntity record);

    int insertSelective(BlogEntity record);

    List<BlogEntity> selectByExampleWithRowbounds(BlogExample example, RowBounds rowBounds);

    List<BlogEntity> selectByExample(BlogExample example);

    BlogEntity selectByPrimaryKey(BlogKey key);

    int updateByExampleSelective(@Param("record") BlogEntity record, @Param("example") BlogExample example);

    int updateByExample(@Param("record") BlogEntity record, @Param("example") BlogExample example);

    int updateByPrimaryKeySelective(BlogEntity record);

    int updateByPrimaryKey(BlogEntity record);

    int batchUpdate(List<BlogEntity> list);

    int batchInsert(List<BlogEntity> list);

    int upsert(@Param("record") BlogEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") BlogEntity record, @Param("array") String[] array);
}