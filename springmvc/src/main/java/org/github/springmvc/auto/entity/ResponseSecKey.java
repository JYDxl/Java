/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import org.github.spring.footstone.Entity;

/**
 * ResponseSecKey [ t_response_sec ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class ResponseSecKey extends Entity implements Serializable {
    /** 二级回复ID编号（主键）(t_response_sec.respond_id). */
    private String respondId;

    private static final long serialVersionUID = 1L;

    public ResponseSecKey(String respondId) {
        this.respondId = respondId;
    }

    public ResponseSecKey() {
        super();
    }

    /**
     * GET 二级回复ID编号（主键）(t_response_sec.respond_id).
     *
     * @return respondId VARCHAR
     */
    public String getRespondId() {
        return respondId;
    }

    public ResponseSecKey withRespondId(String respondId) {
        this.setRespondId(respondId);
        return this;
    }

    /**
     * SET 二级回复ID编号（主键）(t_response_sec.respond_id).
     *
     * @param respondId VARCHAR
     */
    public void setRespondId(String respondId) {
        this.respondId = respondId == null ? null : respondId.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", respondId=").append(respondId);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ResponseSecKey other = (ResponseSecKey) that;
        return (this.getRespondId() == null ? other.getRespondId() == null : this.getRespondId().equals(other.getRespondId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRespondId() == null) ? 0 : getRespondId().hashCode());
        return result;
    }
}