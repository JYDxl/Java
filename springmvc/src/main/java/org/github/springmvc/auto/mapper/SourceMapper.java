/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.SourceEntity;
import org.github.springmvc.auto.entity.SourceExample;
import org.github.springmvc.auto.entity.SourceKey;

@QueryInterface
public interface SourceMapper extends Interface {
    long countByExample(SourceExample example);

    int deleteByExample(SourceExample example);

    int deleteByPrimaryKey(SourceKey key);

    int insert(SourceEntity record);

    int insertSelective(SourceEntity record);

    List<SourceEntity> selectByExampleWithRowbounds(SourceExample example, RowBounds rowBounds);

    List<SourceEntity> selectByExample(SourceExample example);

    SourceEntity selectByPrimaryKey(SourceKey key);

    int updateByExampleSelective(@Param("record") SourceEntity record, @Param("example") SourceExample example);

    int updateByExample(@Param("record") SourceEntity record, @Param("example") SourceExample example);

    int updateByPrimaryKeySelective(SourceEntity record);

    int updateByPrimaryKey(SourceEntity record);

    int batchUpdate(List<SourceEntity> list);

    int batchInsert(List<SourceEntity> list);

    int upsert(@Param("record") SourceEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") SourceEntity record, @Param("array") String[] array);
}