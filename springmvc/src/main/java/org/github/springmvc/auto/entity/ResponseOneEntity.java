/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import java.util.Date;

/**
 * ResponseOneEntity [ t_response_one ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class ResponseOneEntity extends ResponseOneKey implements Serializable {
    /** 用户ID（外键）(t_response_one.user_id). */
    private String userId;

    /** 文章ID（外键）(t_response_one.tid). */
    private String tid;

    /** 回复消息(t_response_one.text). */
    private String text;

    /** 二级回复ID（值唯一）(t_response_one.respond). */
    private String respond;

    /** 创建时间(t_response_one.create_time). */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public ResponseOneEntity(String postId, String userId, String tid, String text, String respond, Date createTime) {
        super(postId);
        this.userId = userId;
        this.tid = tid;
        this.text = text;
        this.respond = respond;
        this.createTime = createTime;
    }

    public ResponseOneEntity() {
        super();
    }

    /**
     * GET 用户ID（外键）(t_response_one.user_id).
     *
     * @return userId VARCHAR
     */
    public String getUserId() {
        return userId;
    }

    public ResponseOneEntity withUserId(String userId) {
        this.setUserId(userId);
        return this;
    }

    /**
     * SET 用户ID（外键）(t_response_one.user_id).
     *
     * @param userId VARCHAR
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * GET 文章ID（外键）(t_response_one.tid).
     *
     * @return tid VARCHAR
     */
    public String getTid() {
        return tid;
    }

    public ResponseOneEntity withTid(String tid) {
        this.setTid(tid);
        return this;
    }

    /**
     * SET 文章ID（外键）(t_response_one.tid).
     *
     * @param tid VARCHAR
     */
    public void setTid(String tid) {
        this.tid = tid == null ? null : tid.trim();
    }

    /**
     * GET 回复消息(t_response_one.text).
     *
     * @return text VARCHAR
     */
    public String getText() {
        return text;
    }

    public ResponseOneEntity withText(String text) {
        this.setText(text);
        return this;
    }

    /**
     * SET 回复消息(t_response_one.text).
     *
     * @param text VARCHAR
     */
    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    /**
     * GET 二级回复ID（值唯一）(t_response_one.respond).
     *
     * @return respond VARCHAR
     */
    public String getRespond() {
        return respond;
    }

    public ResponseOneEntity withRespond(String respond) {
        this.setRespond(respond);
        return this;
    }

    /**
     * SET 二级回复ID（值唯一）(t_response_one.respond).
     *
     * @param respond VARCHAR
     */
    public void setRespond(String respond) {
        this.respond = respond == null ? null : respond.trim();
    }

    /**
     * GET 创建时间(t_response_one.create_time).
     *
     * @return createTime TIMESTAMP
     */
    public Date getCreateTime() {
        return createTime;
    }

    public ResponseOneEntity withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    /**
     * SET 创建时间(t_response_one.create_time).
     *
     * @param createTime TIMESTAMP
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userId=").append(userId);
        sb.append(", tid=").append(tid);
        sb.append(", text=").append(text);
        sb.append(", respond=").append(respond);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ResponseOneEntity other = (ResponseOneEntity) that;
        return (this.getPostId() == null ? other.getPostId() == null : this.getPostId().equals(other.getPostId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getTid() == null ? other.getTid() == null : this.getTid().equals(other.getTid()))
            && (this.getText() == null ? other.getText() == null : this.getText().equals(other.getText()))
            && (this.getRespond() == null ? other.getRespond() == null : this.getRespond().equals(other.getRespond()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPostId() == null) ? 0 : getPostId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getTid() == null) ? 0 : getTid().hashCode());
        result = prime * result + ((getText() == null) ? 0 : getText().hashCode());
        result = prime * result + ((getRespond() == null) ? 0 : getRespond().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        return result;
    }
}