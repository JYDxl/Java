/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import org.github.spring.footstone.Entity;

/**
 * PhotoKey [ photo ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class PhotoKey extends Entity implements Serializable {
    /** id(photo.id). */
    private Integer id;

    private static final long serialVersionUID = 1L;

    public PhotoKey(Integer id) {
        this.id = id;
    }

    public PhotoKey() {
        super();
    }

    /**
     * GET id(photo.id).
     *
     * @return id INTEGER
     */
    public Integer getId() {
        return id;
    }

    public PhotoKey withId(Integer id) {
        this.setId(id);
        return this;
    }

    /**
     * SET id(photo.id).
     *
     * @param id INTEGER
     */
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PhotoKey other = (PhotoKey) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }
}