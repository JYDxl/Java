/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.ArtStatEntity;
import org.github.springmvc.auto.entity.ArtStatExample;
import org.github.springmvc.auto.entity.ArtStatKey;

@QueryInterface
public interface ArtStatMapper extends Interface {
    long countByExample(ArtStatExample example);

    int deleteByExample(ArtStatExample example);

    int deleteByPrimaryKey(ArtStatKey key);

    int insert(ArtStatEntity record);

    int insertSelective(ArtStatEntity record);

    List<ArtStatEntity> selectByExampleWithRowbounds(ArtStatExample example, RowBounds rowBounds);

    List<ArtStatEntity> selectByExample(ArtStatExample example);

    ArtStatEntity selectByPrimaryKey(ArtStatKey key);

    int updateByExampleSelective(@Param("record") ArtStatEntity record, @Param("example") ArtStatExample example);

    int updateByExample(@Param("record") ArtStatEntity record, @Param("example") ArtStatExample example);

    int updateByPrimaryKeySelective(ArtStatEntity record);

    int updateByPrimaryKey(ArtStatEntity record);

    int batchUpdate(List<ArtStatEntity> list);

    int batchInsert(List<ArtStatEntity> list);

    int upsert(@Param("record") ArtStatEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") ArtStatEntity record, @Param("array") String[] array);
}