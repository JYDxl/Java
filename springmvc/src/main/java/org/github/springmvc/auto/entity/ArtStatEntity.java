/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;

/**
 * ArtStatEntity [ t_art_stat ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class ArtStatEntity extends ArtStatKey implements Serializable {
    /** 文章点击数(t_art_stat.read_count). */
    private Integer readCount;

    /** 点赞数(t_art_stat.upvote). */
    private Integer upvote;

    private static final long serialVersionUID = 1L;

    public ArtStatEntity(String tid, Integer readCount, Integer upvote) {
        super(tid);
        this.readCount = readCount;
        this.upvote = upvote;
    }

    public ArtStatEntity() {
        super();
    }

    /**
     * GET 文章点击数(t_art_stat.read_count).
     *
     * @return readCount INTEGER
     */
    public Integer getReadCount() {
        return readCount;
    }

    public ArtStatEntity withReadCount(Integer readCount) {
        this.setReadCount(readCount);
        return this;
    }

    /**
     * SET 文章点击数(t_art_stat.read_count).
     *
     * @param readCount INTEGER
     */
    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    /**
     * GET 点赞数(t_art_stat.upvote).
     *
     * @return upvote INTEGER
     */
    public Integer getUpvote() {
        return upvote;
    }

    public ArtStatEntity withUpvote(Integer upvote) {
        this.setUpvote(upvote);
        return this;
    }

    /**
     * SET 点赞数(t_art_stat.upvote).
     *
     * @param upvote INTEGER
     */
    public void setUpvote(Integer upvote) {
        this.upvote = upvote;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", readCount=").append(readCount);
        sb.append(", upvote=").append(upvote);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ArtStatEntity other = (ArtStatEntity) that;
        return (this.getTid() == null ? other.getTid() == null : this.getTid().equals(other.getTid()))
            && (this.getReadCount() == null ? other.getReadCount() == null : this.getReadCount().equals(other.getReadCount()))
            && (this.getUpvote() == null ? other.getUpvote() == null : this.getUpvote().equals(other.getUpvote()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getTid() == null) ? 0 : getTid().hashCode());
        result = prime * result + ((getReadCount() == null) ? 0 : getReadCount().hashCode());
        result = prime * result + ((getUpvote() == null) ? 0 : getUpvote().hashCode());
        return result;
    }
}