/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.UsersInfoEntity;
import org.github.springmvc.auto.entity.UsersInfoExample;
import org.github.springmvc.auto.entity.UsersInfoKey;

@QueryInterface
public interface UsersInfoMapper extends Interface {
    long countByExample(UsersInfoExample example);

    int deleteByExample(UsersInfoExample example);

    int deleteByPrimaryKey(UsersInfoKey key);

    int insert(UsersInfoEntity record);

    int insertSelective(UsersInfoEntity record);

    List<UsersInfoEntity> selectByExampleWithRowbounds(UsersInfoExample example, RowBounds rowBounds);

    List<UsersInfoEntity> selectByExample(UsersInfoExample example);

    UsersInfoEntity selectByPrimaryKey(UsersInfoKey key);

    int updateByExampleSelective(@Param("record") UsersInfoEntity record, @Param("example") UsersInfoExample example);

    int updateByExample(@Param("record") UsersInfoEntity record, @Param("example") UsersInfoExample example);

    int updateByPrimaryKeySelective(UsersInfoEntity record);

    int updateByPrimaryKey(UsersInfoEntity record);

    int batchUpdate(List<UsersInfoEntity> list);

    int batchInsert(List<UsersInfoEntity> list);

    int upsert(@Param("record") UsersInfoEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") UsersInfoEntity record, @Param("array") String[] array);
}