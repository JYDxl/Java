/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.ResponseOneEntity;
import org.github.springmvc.auto.entity.ResponseOneExample;
import org.github.springmvc.auto.entity.ResponseOneKey;

@QueryInterface
public interface ResponseOneMapper extends Interface {
    long countByExample(ResponseOneExample example);

    int deleteByExample(ResponseOneExample example);

    int deleteByPrimaryKey(ResponseOneKey key);

    int insert(ResponseOneEntity record);

    int insertSelective(ResponseOneEntity record);

    List<ResponseOneEntity> selectByExampleWithRowbounds(ResponseOneExample example, RowBounds rowBounds);

    List<ResponseOneEntity> selectByExample(ResponseOneExample example);

    ResponseOneEntity selectByPrimaryKey(ResponseOneKey key);

    int updateByExampleSelective(@Param("record") ResponseOneEntity record, @Param("example") ResponseOneExample example);

    int updateByExample(@Param("record") ResponseOneEntity record, @Param("example") ResponseOneExample example);

    int updateByPrimaryKeySelective(ResponseOneEntity record);

    int updateByPrimaryKey(ResponseOneEntity record);

    int batchUpdate(List<ResponseOneEntity> list);

    int batchInsert(List<ResponseOneEntity> list);

    int upsert(@Param("record") ResponseOneEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") ResponseOneEntity record, @Param("array") String[] array);
}