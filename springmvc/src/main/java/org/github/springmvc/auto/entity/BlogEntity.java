/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import java.util.Date;

/**
 * BlogEntity [ t_blog ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class BlogEntity extends BlogKey implements Serializable {
    /** 用户ID（外键）(t_blog.user_id). */
    private String userId;

    /** 文章标题(t_blog.title). */
    private String title;

    /** 作者(t_blog.author). */
    private String author;

    /** 文章类型（原创、转载）(t_blog.type). */
    private String type;

    /** 转载地址(t_blog.loadURL). */
    private String loadurl;

    /** 文章标签(t_blog.label). */
    private String label;

    /** 文章描述信息(t_blog.decoration). */
    private String decoration;

    /** 创建时间(t_blog.create_time). */
    private Date createTime;

    /** 修改时间(t_blog.alter_time). */
    private Date alterTime;

    /** 文章状态（未审核、保存、审核通过）(t_blog.state). */
    private String state;

    private static final long serialVersionUID = 1L;

    public BlogEntity(String tid, String userId, String title, String author, String type, String loadurl, String label, String decoration, Date createTime, Date alterTime, String state) {
        super(tid);
        this.userId = userId;
        this.title = title;
        this.author = author;
        this.type = type;
        this.loadurl = loadurl;
        this.label = label;
        this.decoration = decoration;
        this.createTime = createTime;
        this.alterTime = alterTime;
        this.state = state;
    }

    public BlogEntity() {
        super();
    }

    /**
     * GET 用户ID（外键）(t_blog.user_id).
     *
     * @return userId VARCHAR
     */
    public String getUserId() {
        return userId;
    }

    public BlogEntity withUserId(String userId) {
        this.setUserId(userId);
        return this;
    }

    /**
     * SET 用户ID（外键）(t_blog.user_id).
     *
     * @param userId VARCHAR
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * GET 文章标题(t_blog.title).
     *
     * @return title VARCHAR
     */
    public String getTitle() {
        return title;
    }

    public BlogEntity withTitle(String title) {
        this.setTitle(title);
        return this;
    }

    /**
     * SET 文章标题(t_blog.title).
     *
     * @param title VARCHAR
     */
    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    /**
     * GET 作者(t_blog.author).
     *
     * @return author VARCHAR
     */
    public String getAuthor() {
        return author;
    }

    public BlogEntity withAuthor(String author) {
        this.setAuthor(author);
        return this;
    }

    /**
     * SET 作者(t_blog.author).
     *
     * @param author VARCHAR
     */
    public void setAuthor(String author) {
        this.author = author == null ? null : author.trim();
    }

    /**
     * GET 文章类型（原创、转载）(t_blog.type).
     *
     * @return type CHAR
     */
    public String getType() {
        return type;
    }

    public BlogEntity withType(String type) {
        this.setType(type);
        return this;
    }

    /**
     * SET 文章类型（原创、转载）(t_blog.type).
     *
     * @param type CHAR
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * GET 转载地址(t_blog.loadURL).
     *
     * @return loadurl VARCHAR
     */
    public String getLoadurl() {
        return loadurl;
    }

    public BlogEntity withLoadurl(String loadurl) {
        this.setLoadurl(loadurl);
        return this;
    }

    /**
     * SET 转载地址(t_blog.loadURL).
     *
     * @param loadurl VARCHAR
     */
    public void setLoadurl(String loadurl) {
        this.loadurl = loadurl == null ? null : loadurl.trim();
    }

    /**
     * GET 文章标签(t_blog.label).
     *
     * @return label VARCHAR
     */
    public String getLabel() {
        return label;
    }

    public BlogEntity withLabel(String label) {
        this.setLabel(label);
        return this;
    }

    /**
     * SET 文章标签(t_blog.label).
     *
     * @param label VARCHAR
     */
    public void setLabel(String label) {
        this.label = label == null ? null : label.trim();
    }

    /**
     * GET 文章描述信息(t_blog.decoration).
     *
     * @return decoration VARCHAR
     */
    public String getDecoration() {
        return decoration;
    }

    public BlogEntity withDecoration(String decoration) {
        this.setDecoration(decoration);
        return this;
    }

    /**
     * SET 文章描述信息(t_blog.decoration).
     *
     * @param decoration VARCHAR
     */
    public void setDecoration(String decoration) {
        this.decoration = decoration == null ? null : decoration.trim();
    }

    /**
     * GET 创建时间(t_blog.create_time).
     *
     * @return createTime TIMESTAMP
     */
    public Date getCreateTime() {
        return createTime;
    }

    public BlogEntity withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    /**
     * SET 创建时间(t_blog.create_time).
     *
     * @param createTime TIMESTAMP
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * GET 修改时间(t_blog.alter_time).
     *
     * @return alterTime TIMESTAMP
     */
    public Date getAlterTime() {
        return alterTime;
    }

    public BlogEntity withAlterTime(Date alterTime) {
        this.setAlterTime(alterTime);
        return this;
    }

    /**
     * SET 修改时间(t_blog.alter_time).
     *
     * @param alterTime TIMESTAMP
     */
    public void setAlterTime(Date alterTime) {
        this.alterTime = alterTime;
    }

    /**
     * GET 文章状态（未审核、保存、审核通过）(t_blog.state).
     *
     * @return state CHAR
     */
    public String getState() {
        return state;
    }

    public BlogEntity withState(String state) {
        this.setState(state);
        return this;
    }

    /**
     * SET 文章状态（未审核、保存、审核通过）(t_blog.state).
     *
     * @param state CHAR
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userId=").append(userId);
        sb.append(", title=").append(title);
        sb.append(", author=").append(author);
        sb.append(", type=").append(type);
        sb.append(", loadurl=").append(loadurl);
        sb.append(", label=").append(label);
        sb.append(", decoration=").append(decoration);
        sb.append(", createTime=").append(createTime);
        sb.append(", alterTime=").append(alterTime);
        sb.append(", state=").append(state);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        BlogEntity other = (BlogEntity) that;
        return (this.getTid() == null ? other.getTid() == null : this.getTid().equals(other.getTid()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
            && (this.getAuthor() == null ? other.getAuthor() == null : this.getAuthor().equals(other.getAuthor()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getLoadurl() == null ? other.getLoadurl() == null : this.getLoadurl().equals(other.getLoadurl()))
            && (this.getLabel() == null ? other.getLabel() == null : this.getLabel().equals(other.getLabel()))
            && (this.getDecoration() == null ? other.getDecoration() == null : this.getDecoration().equals(other.getDecoration()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getAlterTime() == null ? other.getAlterTime() == null : this.getAlterTime().equals(other.getAlterTime()))
            && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getTid() == null) ? 0 : getTid().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
        result = prime * result + ((getAuthor() == null) ? 0 : getAuthor().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getLoadurl() == null) ? 0 : getLoadurl().hashCode());
        result = prime * result + ((getLabel() == null) ? 0 : getLabel().hashCode());
        result = prime * result + ((getDecoration() == null) ? 0 : getDecoration().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getAlterTime() == null) ? 0 : getAlterTime().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        return result;
    }
}