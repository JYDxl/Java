/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.PhotoEntity;
import org.github.springmvc.auto.entity.PhotoExample;
import org.github.springmvc.auto.entity.PhotoKey;

@QueryInterface
public interface PhotoMapper extends Interface {
    long countByExample(PhotoExample example);

    int deleteByExample(PhotoExample example);

    int deleteByPrimaryKey(PhotoKey key);

    int insert(PhotoEntity record);

    int insertSelective(PhotoEntity record);

    List<PhotoEntity> selectByExampleWithRowbounds(PhotoExample example, RowBounds rowBounds);

    List<PhotoEntity> selectByExample(PhotoExample example);

    PhotoEntity selectByPrimaryKey(PhotoKey key);

    int updateByExampleSelective(@Param("record") PhotoEntity record, @Param("example") PhotoExample example);

    int updateByExample(@Param("record") PhotoEntity record, @Param("example") PhotoExample example);

    int updateByPrimaryKeySelective(PhotoEntity record);

    int updateByPrimaryKey(PhotoEntity record);

    int batchUpdate(List<PhotoEntity> list);

    int batchInsert(List<PhotoEntity> list);

    int upsert(@Param("record") PhotoEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") PhotoEntity record, @Param("array") String[] array);
}