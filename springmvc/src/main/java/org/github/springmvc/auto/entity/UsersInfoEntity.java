/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;

/**
 * UsersInfoEntity [ t_users_info ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class UsersInfoEntity extends UsersInfoKey implements Serializable {
    /** 用户姓名(t_users_info.name). */
    private String name;

    /** 用户性别('0','1')('男','女')(t_users_info.sex). */
    private String sex;

    /** 生日(t_users_info.birthday). */
    private String birthday;

    /** 邮箱(t_users_info.email). */
    private String email;

    /** 电话(t_users_info.tel_phone). */
    private String telPhone;

    private static final long serialVersionUID = 1L;

    public UsersInfoEntity(String userId, String name, String sex, String birthday, String email, String telPhone) {
        super(userId);
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
        this.email = email;
        this.telPhone = telPhone;
    }

    public UsersInfoEntity() {
        super();
    }

    /**
     * GET 用户姓名(t_users_info.name).
     *
     * @return name VARCHAR
     */
    public String getName() {
        return name;
    }

    public UsersInfoEntity withName(String name) {
        this.setName(name);
        return this;
    }

    /**
     * SET 用户姓名(t_users_info.name).
     *
     * @param name VARCHAR
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * GET 用户性别('0','1')('男','女')(t_users_info.sex).
     *
     * @return sex CHAR
     */
    public String getSex() {
        return sex;
    }

    public UsersInfoEntity withSex(String sex) {
        this.setSex(sex);
        return this;
    }

    /**
     * SET 用户性别('0','1')('男','女')(t_users_info.sex).
     *
     * @param sex CHAR
     */
    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    /**
     * GET 生日(t_users_info.birthday).
     *
     * @return birthday VARCHAR
     */
    public String getBirthday() {
        return birthday;
    }

    public UsersInfoEntity withBirthday(String birthday) {
        this.setBirthday(birthday);
        return this;
    }

    /**
     * SET 生日(t_users_info.birthday).
     *
     * @param birthday VARCHAR
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday == null ? null : birthday.trim();
    }

    /**
     * GET 邮箱(t_users_info.email).
     *
     * @return email VARCHAR
     */
    public String getEmail() {
        return email;
    }

    public UsersInfoEntity withEmail(String email) {
        this.setEmail(email);
        return this;
    }

    /**
     * SET 邮箱(t_users_info.email).
     *
     * @param email VARCHAR
     */
    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    /**
     * GET 电话(t_users_info.tel_phone).
     *
     * @return telPhone VARCHAR
     */
    public String getTelPhone() {
        return telPhone;
    }

    public UsersInfoEntity withTelPhone(String telPhone) {
        this.setTelPhone(telPhone);
        return this;
    }

    /**
     * SET 电话(t_users_info.tel_phone).
     *
     * @param telPhone VARCHAR
     */
    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone == null ? null : telPhone.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", name=").append(name);
        sb.append(", sex=").append(sex);
        sb.append(", birthday=").append(birthday);
        sb.append(", email=").append(email);
        sb.append(", telPhone=").append(telPhone);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UsersInfoEntity other = (UsersInfoEntity) that;
        return (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getSex() == null ? other.getSex() == null : this.getSex().equals(other.getSex()))
            && (this.getBirthday() == null ? other.getBirthday() == null : this.getBirthday().equals(other.getBirthday()))
            && (this.getEmail() == null ? other.getEmail() == null : this.getEmail().equals(other.getEmail()))
            && (this.getTelPhone() == null ? other.getTelPhone() == null : this.getTelPhone().equals(other.getTelPhone()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getSex() == null) ? 0 : getSex().hashCode());
        result = prime * result + ((getBirthday() == null) ? 0 : getBirthday().hashCode());
        result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
        result = prime * result + ((getTelPhone() == null) ? 0 : getTelPhone().hashCode());
        return result;
    }
}