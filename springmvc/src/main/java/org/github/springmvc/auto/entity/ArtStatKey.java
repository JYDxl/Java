/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import org.github.spring.footstone.Entity;

/**
 * ArtStatKey [ t_art_stat ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class ArtStatKey extends Entity implements Serializable {
    /** 文章ID（主键、外键）(t_art_stat.tid). */
    private String tid;

    private static final long serialVersionUID = 1L;

    public ArtStatKey(String tid) {
        this.tid = tid;
    }

    public ArtStatKey() {
        super();
    }

    /**
     * GET 文章ID（主键、外键）(t_art_stat.tid).
     *
     * @return tid VARCHAR
     */
    public String getTid() {
        return tid;
    }

    public ArtStatKey withTid(String tid) {
        this.setTid(tid);
        return this;
    }

    /**
     * SET 文章ID（主键、外键）(t_art_stat.tid).
     *
     * @param tid VARCHAR
     */
    public void setTid(String tid) {
        this.tid = tid == null ? null : tid.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", tid=").append(tid);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ArtStatKey other = (ArtStatKey) that;
        return (this.getTid() == null ? other.getTid() == null : this.getTid().equals(other.getTid()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getTid() == null) ? 0 : getTid().hashCode());
        return result;
    }
}