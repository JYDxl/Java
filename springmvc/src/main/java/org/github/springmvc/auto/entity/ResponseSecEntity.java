/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import java.util.Date;

/**
 * ResponseSecEntity [ t_response_sec ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class ResponseSecEntity extends ResponseSecKey implements Serializable {
    /** 用户ID（外键）(t_response_sec.user_id). */
    private String userId;

    /** 回复消息(t_response_sec.text). */
    private String text;

    /** 创建时间(t_response_sec.create_time). */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public ResponseSecEntity(String respondId, String userId, String text, Date createTime) {
        super(respondId);
        this.userId = userId;
        this.text = text;
        this.createTime = createTime;
    }

    public ResponseSecEntity() {
        super();
    }

    /**
     * GET 用户ID（外键）(t_response_sec.user_id).
     *
     * @return userId VARCHAR
     */
    public String getUserId() {
        return userId;
    }

    public ResponseSecEntity withUserId(String userId) {
        this.setUserId(userId);
        return this;
    }

    /**
     * SET 用户ID（外键）(t_response_sec.user_id).
     *
     * @param userId VARCHAR
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * GET 回复消息(t_response_sec.text).
     *
     * @return text VARCHAR
     */
    public String getText() {
        return text;
    }

    public ResponseSecEntity withText(String text) {
        this.setText(text);
        return this;
    }

    /**
     * SET 回复消息(t_response_sec.text).
     *
     * @param text VARCHAR
     */
    public void setText(String text) {
        this.text = text == null ? null : text.trim();
    }

    /**
     * GET 创建时间(t_response_sec.create_time).
     *
     * @return createTime TIMESTAMP
     */
    public Date getCreateTime() {
        return createTime;
    }

    public ResponseSecEntity withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    /**
     * SET 创建时间(t_response_sec.create_time).
     *
     * @param createTime TIMESTAMP
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userId=").append(userId);
        sb.append(", text=").append(text);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ResponseSecEntity other = (ResponseSecEntity) that;
        return (this.getRespondId() == null ? other.getRespondId() == null : this.getRespondId().equals(other.getRespondId()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getText() == null ? other.getText() == null : this.getText().equals(other.getText()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getRespondId() == null) ? 0 : getRespondId().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getText() == null) ? 0 : getText().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        return result;
    }
}