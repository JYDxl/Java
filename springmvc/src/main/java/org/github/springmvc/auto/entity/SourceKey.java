/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;
import org.github.spring.footstone.Entity;

/**
 * SourceKey [ t_source ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class SourceKey extends Entity implements Serializable {
    /** 资源ID（主键）(t_source.source_id). */
    private String sourceId;

    private static final long serialVersionUID = 1L;

    public SourceKey(String sourceId) {
        this.sourceId = sourceId;
    }

    public SourceKey() {
        super();
    }

    /**
     * GET 资源ID（主键）(t_source.source_id).
     *
     * @return sourceId VARCHAR
     */
    public String getSourceId() {
        return sourceId;
    }

    public SourceKey withSourceId(String sourceId) {
        this.setSourceId(sourceId);
        return this;
    }

    /**
     * SET 资源ID（主键）(t_source.source_id).
     *
     * @param sourceId VARCHAR
     */
    public void setSourceId(String sourceId) {
        this.sourceId = sourceId == null ? null : sourceId.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sourceId=").append(sourceId);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SourceKey other = (SourceKey) that;
        return (this.getSourceId() == null ? other.getSourceId() == null : this.getSourceId().equals(other.getSourceId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getSourceId() == null) ? 0 : getSourceId().hashCode());
        return result;
    }
}