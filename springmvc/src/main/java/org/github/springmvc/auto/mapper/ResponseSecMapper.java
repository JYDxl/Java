/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.ResponseSecEntity;
import org.github.springmvc.auto.entity.ResponseSecExample;
import org.github.springmvc.auto.entity.ResponseSecKey;

@QueryInterface
public interface ResponseSecMapper extends Interface {
    long countByExample(ResponseSecExample example);

    int deleteByExample(ResponseSecExample example);

    int deleteByPrimaryKey(ResponseSecKey key);

    int insert(ResponseSecEntity record);

    int insertSelective(ResponseSecEntity record);

    List<ResponseSecEntity> selectByExampleWithRowbounds(ResponseSecExample example, RowBounds rowBounds);

    List<ResponseSecEntity> selectByExample(ResponseSecExample example);

    ResponseSecEntity selectByPrimaryKey(ResponseSecKey key);

    int updateByExampleSelective(@Param("record") ResponseSecEntity record, @Param("example") ResponseSecExample example);

    int updateByExample(@Param("record") ResponseSecEntity record, @Param("example") ResponseSecExample example);

    int updateByPrimaryKeySelective(ResponseSecEntity record);

    int updateByPrimaryKey(ResponseSecEntity record);

    int batchUpdate(List<ResponseSecEntity> list);

    int batchInsert(List<ResponseSecEntity> list);

    int upsert(@Param("record") ResponseSecEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") ResponseSecEntity record, @Param("array") String[] array);
}