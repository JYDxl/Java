/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.CritiqueEntity;
import org.github.springmvc.auto.entity.CritiqueExample;
import org.github.springmvc.auto.entity.CritiqueKey;

@QueryInterface
public interface CritiqueMapper extends Interface {
    long countByExample(CritiqueExample example);

    int deleteByExample(CritiqueExample example);

    int deleteByPrimaryKey(CritiqueKey key);

    int insert(CritiqueEntity record);

    int insertSelective(CritiqueEntity record);

    List<CritiqueEntity> selectByExampleWithRowbounds(CritiqueExample example, RowBounds rowBounds);

    List<CritiqueEntity> selectByExample(CritiqueExample example);

    CritiqueEntity selectByPrimaryKey(CritiqueKey key);

    int updateByExampleSelective(@Param("record") CritiqueEntity record, @Param("example") CritiqueExample example);

    int updateByExample(@Param("record") CritiqueEntity record, @Param("example") CritiqueExample example);

    int updateByPrimaryKeySelective(CritiqueEntity record);

    int updateByPrimaryKey(CritiqueEntity record);

    int batchUpdate(List<CritiqueEntity> list);

    int batchInsert(List<CritiqueEntity> list);

    int upsert(@Param("record") CritiqueEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") CritiqueEntity record, @Param("array") String[] array);
}