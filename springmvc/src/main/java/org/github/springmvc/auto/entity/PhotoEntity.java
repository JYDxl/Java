/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;

/**
 * PhotoEntity [ photo ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class PhotoEntity extends PhotoKey implements Serializable {
    /** image(photo.image). */
    private String image;

    /** note(photo.note). */
    private String note;

    /** content(photo.content). */
    private String content;

    /** type(photo.type). */
    private String type;

    /** notice(photo.notice). */
    private String notice;

    /** time(photo.time). */
    private String time;

    /** album_id(photo.album_id). */
    private Integer albumId;

    private static final long serialVersionUID = 1L;

    public PhotoEntity(Integer id, String image, String note, String content, String type, String notice, String time, Integer albumId) {
        super(id);
        this.image = image;
        this.note = note;
        this.content = content;
        this.type = type;
        this.notice = notice;
        this.time = time;
        this.albumId = albumId;
    }

    public PhotoEntity() {
        super();
    }

    /**
     * GET image(photo.image).
     *
     * @return image VARCHAR
     */
    public String getImage() {
        return image;
    }

    public PhotoEntity withImage(String image) {
        this.setImage(image);
        return this;
    }

    /**
     * SET image(photo.image).
     *
     * @param image VARCHAR
     */
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /**
     * GET note(photo.note).
     *
     * @return note VARCHAR
     */
    public String getNote() {
        return note;
    }

    public PhotoEntity withNote(String note) {
        this.setNote(note);
        return this;
    }

    /**
     * SET note(photo.note).
     *
     * @param note VARCHAR
     */
    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    /**
     * GET content(photo.content).
     *
     * @return content VARCHAR
     */
    public String getContent() {
        return content;
    }

    public PhotoEntity withContent(String content) {
        this.setContent(content);
        return this;
    }

    /**
     * SET content(photo.content).
     *
     * @param content VARCHAR
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * GET type(photo.type).
     *
     * @return type VARCHAR
     */
    public String getType() {
        return type;
    }

    public PhotoEntity withType(String type) {
        this.setType(type);
        return this;
    }

    /**
     * SET type(photo.type).
     *
     * @param type VARCHAR
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * GET notice(photo.notice).
     *
     * @return notice VARCHAR
     */
    public String getNotice() {
        return notice;
    }

    public PhotoEntity withNotice(String notice) {
        this.setNotice(notice);
        return this;
    }

    /**
     * SET notice(photo.notice).
     *
     * @param notice VARCHAR
     */
    public void setNotice(String notice) {
        this.notice = notice == null ? null : notice.trim();
    }

    /**
     * GET time(photo.time).
     *
     * @return time VARCHAR
     */
    public String getTime() {
        return time;
    }

    public PhotoEntity withTime(String time) {
        this.setTime(time);
        return this;
    }

    /**
     * SET time(photo.time).
     *
     * @param time VARCHAR
     */
    public void setTime(String time) {
        this.time = time == null ? null : time.trim();
    }

    /**
     * GET album_id(photo.album_id).
     *
     * @return albumId INTEGER
     */
    public Integer getAlbumId() {
        return albumId;
    }

    public PhotoEntity withAlbumId(Integer albumId) {
        this.setAlbumId(albumId);
        return this;
    }

    /**
     * SET album_id(photo.album_id).
     *
     * @param albumId INTEGER
     */
    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", image=").append(image);
        sb.append(", note=").append(note);
        sb.append(", content=").append(content);
        sb.append(", type=").append(type);
        sb.append(", notice=").append(notice);
        sb.append(", time=").append(time);
        sb.append(", albumId=").append(albumId);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PhotoEntity other = (PhotoEntity) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getImage() == null ? other.getImage() == null : this.getImage().equals(other.getImage()))
            && (this.getNote() == null ? other.getNote() == null : this.getNote().equals(other.getNote()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getNotice() == null ? other.getNotice() == null : this.getNotice().equals(other.getNotice()))
            && (this.getTime() == null ? other.getTime() == null : this.getTime().equals(other.getTime()))
            && (this.getAlbumId() == null ? other.getAlbumId() == null : this.getAlbumId().equals(other.getAlbumId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getImage() == null) ? 0 : getImage().hashCode());
        result = prime * result + ((getNote() == null) ? 0 : getNote().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getNotice() == null) ? 0 : getNotice().hashCode());
        result = prime * result + ((getTime() == null) ? 0 : getTime().hashCode());
        result = prime * result + ((getAlbumId() == null) ? 0 : getAlbumId().hashCode());
        return result;
    }
}