/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.entity;

import com.mysema.query.annotations.QueryEntity;
import java.io.Serializable;

/**
 * MeEntity [ me ].
 *
 * @author JYD_XL
 * @version 2017-05-25
*/
@QueryEntity
public class MeEntity extends MeKey implements Serializable {
    /** content(me.content). */
    private String content;

    /** name(me.name). */
    private String name;

    /** notice(me.notice). */
    private String notice;

    private static final long serialVersionUID = 1L;

    public MeEntity(Integer id, String content, String name, String notice) {
        super(id);
        this.content = content;
        this.name = name;
        this.notice = notice;
    }

    public MeEntity() {
        super();
    }

    /**
     * GET content(me.content).
     *
     * @return content VARCHAR
     */
    public String getContent() {
        return content;
    }

    public MeEntity withContent(String content) {
        this.setContent(content);
        return this;
    }

    /**
     * SET content(me.content).
     *
     * @param content VARCHAR
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * GET name(me.name).
     *
     * @return name VARCHAR
     */
    public String getName() {
        return name;
    }

    public MeEntity withName(String name) {
        this.setName(name);
        return this;
    }

    /**
     * SET name(me.name).
     *
     * @param name VARCHAR
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * GET notice(me.notice).
     *
     * @return notice VARCHAR
     */
    public String getNotice() {
        return notice;
    }

    public MeEntity withNotice(String notice) {
        this.setNotice(notice);
        return this;
    }

    /**
     * SET notice(me.notice).
     *
     * @param notice VARCHAR
     */
    public void setNotice(String notice) {
        this.notice = notice == null ? null : notice.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", content=").append(content);
        sb.append(", name=").append(name);
        sb.append(", notice=").append(notice);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        MeEntity other = (MeEntity) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getContent() == null ? other.getContent() == null : this.getContent().equals(other.getContent()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getNotice() == null ? other.getNotice() == null : this.getNotice().equals(other.getNotice()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getNotice() == null) ? 0 : getNotice().hashCode());
        return result;
    }
}