/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.UsersEntity;
import org.github.springmvc.auto.entity.UsersExample;
import org.github.springmvc.auto.entity.UsersKey;

@QueryInterface
public interface UsersMapper extends Interface {
    long countByExample(UsersExample example);

    int deleteByExample(UsersExample example);

    int deleteByPrimaryKey(UsersKey key);

    int insert(UsersEntity record);

    int insertSelective(UsersEntity record);

    List<UsersEntity> selectByExampleWithRowbounds(UsersExample example, RowBounds rowBounds);

    List<UsersEntity> selectByExample(UsersExample example);

    UsersEntity selectByPrimaryKey(UsersKey key);

    int updateByExampleSelective(@Param("record") UsersEntity record, @Param("example") UsersExample example);

    int updateByExample(@Param("record") UsersEntity record, @Param("example") UsersExample example);

    int updateByPrimaryKeySelective(UsersEntity record);

    int updateByPrimaryKey(UsersEntity record);

    int batchUpdate(List<UsersEntity> list);

    int batchInsert(List<UsersEntity> list);

    int upsert(@Param("record") UsersEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") UsersEntity record, @Param("array") String[] array);
}