/** Created by JYD_XL ON [ 2017-05-25 ]. */
package org.github.springmvc.auto.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.Interface;
import org.github.spring.system.annotation.QueryInterface;
import org.github.springmvc.auto.entity.MeEntity;
import org.github.springmvc.auto.entity.MeExample;
import org.github.springmvc.auto.entity.MeKey;

@QueryInterface
public interface MeMapper extends Interface {
    long countByExample(MeExample example);

    int deleteByExample(MeExample example);

    int deleteByPrimaryKey(MeKey key);

    int insert(MeEntity record);

    int insertSelective(MeEntity record);

    List<MeEntity> selectByExampleWithRowbounds(MeExample example, RowBounds rowBounds);

    List<MeEntity> selectByExample(MeExample example);

    MeEntity selectByPrimaryKey(MeKey key);

    int updateByExampleSelective(@Param("record") MeEntity record, @Param("example") MeExample example);

    int updateByExample(@Param("record") MeEntity record, @Param("example") MeExample example);

    int updateByPrimaryKeySelective(MeEntity record);

    int updateByPrimaryKey(MeEntity record);

    int batchUpdate(List<MeEntity> list);

    int batchInsert(List<MeEntity> list);

    int upsert(@Param("record") MeEntity record, @Param("array") String[] array);

    int upsertSelective(@Param("record") MeEntity record, @Param("array") String[] array);
}