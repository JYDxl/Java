package org.github.spring.system.resolver;

import org.github.spring.system.annotation.ViewResolver;
import org.springframework.core.annotation.Order;

/**
 * ViewResolver.
 *
 * @author JYD_XL
 */
@Order(0)
@ViewResolver
public class HTMLViewResolver extends AbstractViewResolver {
    @Override
    public void afterPropertiesSet() throws Exception {
        this.setViewNames(HTML_VIEW_NAMES);
        this.setContentType(DEFAULT_CONTENT_TYPE);
        this.setPrefix(DEFAULT_PREFIX);
        this.setSuffix(SUFFIX_HTML);
    }
}