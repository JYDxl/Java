package org.github.spring.system;

import org.github.spring.footstone.AbstractComponent;
import org.springframework.beans.factory.FactoryBean;

/**
 * LocaleFactoryBean.
 * 
 * @author JYD_XL
 */
public class LocaleFactoryBean extends AbstractComponent implements FactoryBean<String> {
    @Override
    public String getObject() throws Exception {
        return null;
    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}