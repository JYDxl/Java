package org.github.spring.system.listener;

import org.springframework.web.util.IntrospectorCleanupListener;

/**
 * Created by hanjian on 2017/5/25.
 */
public class SpringCleanupListener extends IntrospectorCleanupListener {}
