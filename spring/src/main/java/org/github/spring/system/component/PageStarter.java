package org.github.spring.system.component;

import com.github.pagehelper.PageHelper;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.footstone.AbstractCondModel;
import org.springframework.stereotype.Component;

import static org.github.spring.footstone.Entity.QUERY;
import static org.github.spring.footstone.Interface.BEAN_PAGE_STARTER;

/**
 * PAGEStarter.
 *
 * @author JYD_XL
 * @version 1.0
 * @see com.github.pagehelper.PageHelper
 * @see com.github.pagehelper.BasePageHelper
 * @see org.apache.ibatis.plugin.Interceptor
 * @see org.springframework.beans.factory.InitializingBean
 */
@Component(BEAN_PAGE_STARTER)
@Intercepts(@Signature(type = Executor.class, method = QUERY, args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}))
public class PageStarter extends PageHelper {
    /**
     * 开始分页.
     *
     * @param condModel 查询参数数据模型
     */
    public static void startPage(AbstractCondModel condModel) {
        startPage(condModel.getPageNumber(), condModel.getDataSize(), condModel.getSortInfo());
    }
}