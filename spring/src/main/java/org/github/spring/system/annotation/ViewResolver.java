package org.github.spring.system.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/** Created by JYD_XL on 5/28/17. */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface ViewResolver {}