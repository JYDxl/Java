package org.github.spring.system.servlet;

import org.github.spring.support.factory.LocaleConversionServiceFactoryBean;
import org.github.spring.system.component.ApplicationContextHolder;
import org.github.spring.system.component.ReturnableValueHandler;
import org.github.spring.system.resolver.InvokeArgumentResolver;
import org.github.spring.system.resolver.SystemExceptionResolver;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolverComposite;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.HandlerMethodReturnValueHandlerComposite;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * WebServlet.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see javax.servlet.Servlet
 * @see javax.servlet.ServletConfig
 * @see javax.servlet.GenericServlet
 * @see javax.servlet.http.HttpServlet
 * @see org.springframework.beans.factory.Aware
 * @see org.springframework.context.EnvironmentAware
 * @see org.springframework.core.env.EnvironmentCapable
 * @see org.springframework.web.servlet.HttpServletBean
 * @see org.springframework.web.servlet.FrameworkServlet
 * @see org.springframework.web.servlet.DispatcherServlet
 * @see org.springframework.context.ApplicationContextAware
 * @since 1.0
 */
@SuppressWarnings("serial")
public class WebServlet extends DispatcherServlet {
    private static final String FIELD_HANDLER_ADAPTERS = "handlerAdapters";

    private static final String FIELD_ARGUMENT_RESOLVERS = "argumentResolvers";

    private static final String FIELD_RETURN_VALUE_HANDLERS = "returnValueHandlers";

    /** 系统默认异常处理器. */
    private SystemExceptionResolver systemExceptionResolver = new SystemExceptionResolver();

    @Override
    protected void initStrategies(ApplicationContext applicationContext) {
        super.initStrategies(applicationContext);
        try {
            initOthers();
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException | IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /** 初始化自定义设置. */
    @SuppressWarnings("unchecked")
    private void initOthers() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, NoSuchMethodException, IOException {
        Field handlerAdapters = this.getClass().getSuperclass().getDeclaredField(FIELD_HANDLER_ADAPTERS);
        handlerAdapters.setAccessible(true);
        List<Object> handlerAdapterList = (List<Object>) handlerAdapters.get(this);
        Collections.reverse(handlerAdapterList);
        RequestMappingHandlerAdapter requestMappingHandlerAdapter = (RequestMappingHandlerAdapter) handlerAdapterList.get(0);
        Class<?> clazz = requestMappingHandlerAdapter.getClass();

        Field argumentResolvers = clazz.getDeclaredField(FIELD_ARGUMENT_RESOLVERS);
        argumentResolvers.setAccessible(true);
        HandlerMethodArgumentResolverComposite argumentResolverComposite = (HandlerMethodArgumentResolverComposite) argumentResolvers.get(requestMappingHandlerAdapter);
        Class<?> argumentResolverCompositeClass = argumentResolverComposite.getClass();
        Field argumentResolversLink = argumentResolverCompositeClass.getDeclaredField(FIELD_ARGUMENT_RESOLVERS);
        argumentResolversLink.setAccessible(true);
        List<HandlerMethodArgumentResolver> argumentResolverList = new ArrayList<>((List<HandlerMethodArgumentResolver>) argumentResolversLink.get(argumentResolverComposite));
        argumentResolverList.add(0, new InvokeArgumentResolver());
        argumentResolvers.set(requestMappingHandlerAdapter, new HandlerMethodArgumentResolverComposite().addResolvers(argumentResolverList));

        Field returnValueHandlers = clazz.getDeclaredField(FIELD_RETURN_VALUE_HANDLERS);
        returnValueHandlers.setAccessible(true);
        HandlerMethodReturnValueHandlerComposite returnValueHandlerComposite = (HandlerMethodReturnValueHandlerComposite) returnValueHandlers.get(requestMappingHandlerAdapter);
        Class<?> returnValueHandlerCompositeClass = returnValueHandlerComposite.getClass();
        Field returnValueHandlersLink = returnValueHandlerCompositeClass.getDeclaredField(FIELD_RETURN_VALUE_HANDLERS);
        returnValueHandlersLink.setAccessible(true);
        List<HandlerMethodReturnValueHandler> returnValueHandlerList = new ArrayList<>((List<HandlerMethodReturnValueHandler>) returnValueHandlersLink.get(returnValueHandlerComposite));
        returnValueHandlerList.add(0, new ReturnableValueHandler());
        returnValueHandlers.set(requestMappingHandlerAdapter, new HandlerMethodReturnValueHandlerComposite().addHandlers(returnValueHandlerList));

        ConfigurableWebBindingInitializer webBindingInitializer = (ConfigurableWebBindingInitializer) requestMappingHandlerAdapter.getWebBindingInitializer();
        webBindingInitializer.setConversionService(ApplicationContextHolder.getBean(LocaleConversionServiceFactoryBean.class).getObject());
    }

    @Override
    protected ModelAndView processHandlerException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        return systemExceptionResolver.resolveException(request, response, handler, ex) != null ? null : super.processHandlerException(request, response, handler, ex);
    }
}