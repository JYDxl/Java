package org.github.spring.system.resolver;

import lombok.extern.log4j.Log4j2;
import org.github.spring.footstone.AbstractComponent;
import org.github.spring.javabean.exception.DataException;
import org.github.spring.javabean.exception.InvalidParamsException;
import org.github.spring.javabean.exception.RunException;
import org.github.spring.javabean.restful.Returnable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * SystemExceptionResolver.
 *
 * @author JYD_XL
 */
@Log4j2
public class SystemExceptionResolver extends AbstractComponent {
    public Returnable resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception e) {
        try {
            if (e instanceof InvalidParamsException) {} else if (e instanceof RunException) {
                //
            } else if (e instanceof DataException) {
                //
            } else if (e instanceof RuntimeException) {
                //
            } else {
                //
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }
}