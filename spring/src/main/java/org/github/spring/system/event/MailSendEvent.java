package org.github.spring.system.event;

import org.github.spring.system.component.ApplicationContextHolder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ApplicationContextEvent;

/**
 * MailSendEvent.
 * 
 * @author JYD_XL
 */
@SuppressWarnings("serial")
public class MailSendEvent extends ApplicationContextEvent {
    /** Constructor. */
    public MailSendEvent() {
        super(ApplicationContextHolder.getApplicationContext());
    }

    /**
     * Constructor.
     *
     * @param applicationContext
     */
    public MailSendEvent(ApplicationContext applicationContext) {
        super(applicationContext);
    }
}