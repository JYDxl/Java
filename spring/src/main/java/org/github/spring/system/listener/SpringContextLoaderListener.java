package org.github.spring.system.listener;

import org.springframework.web.context.ContextLoaderListener;

/**
 * SpringContextLoaderListener.
 *
 * @author JYD_XL
 */
public class SpringContextLoaderListener extends ContextLoaderListener {}