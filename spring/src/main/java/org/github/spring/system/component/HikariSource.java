package org.github.spring.system.component;

import com.zaxxer.hikari.HikariDataSource;
import org.github.spring.support.LocaleProperties;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;

/**
 * MyBatis数据源，配合MYSQL使用.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.sql.Wrapper
 * @see java.io.Closeable
 * @see javax.sql.DataSource
 * @see java.lang.AutoCloseable
 * @see javax.sql.CommonDataSource
 * @see com.zaxxer.hikari.HikariConfig
 * @see com.zaxxer.hikari.HikariDataSource
 * @see com.zaxxer.hikari.HikariConfigMXBean
 */
public class HikariSource extends HikariDataSource implements DataSource, InitializingBean {
    private Resource dataSourceResource;
    private Resource healthCheckResource;

    @Override
    @PreDestroy
    public void close() {
        super.close();
    }

    public void setDataSourceResource(Resource dataSourceProperties) {
        this.dataSourceResource = dataSourceProperties;
    }

    public void setHealthCheckResource(Resource healthCheckProperties) {
        this.healthCheckResource = healthCheckProperties;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.setDataSourceProperties(new LocaleProperties(dataSourceResource));
        this.setHealthCheckProperties(new LocaleProperties(healthCheckResource));
    }
}