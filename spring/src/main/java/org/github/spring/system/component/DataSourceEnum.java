package org.github.spring.system.component;

import java.util.function.Supplier;

/**
 * Created by hanjian on 2017/5/27.
 */
public enum DataSourceEnum implements Supplier<String> {
    MASTER("master"), SLAVE("slave");

    private final String _dataSource;

    DataSourceEnum(String dataSource) {
        _dataSource = dataSource;
    }

    @Override
    public String get() {
        return _dataSource;
    }
}