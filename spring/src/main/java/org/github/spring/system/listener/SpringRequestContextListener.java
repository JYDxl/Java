package org.github.spring.system.listener;

import org.springframework.web.context.request.RequestContextListener;

/**
 * SpringRequestContextListener.
 * 
 * @author JYD_XL
 */
public class SpringRequestContextListener extends RequestContextListener {}