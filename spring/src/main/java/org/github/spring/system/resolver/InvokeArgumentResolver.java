package org.github.spring.system.resolver;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.github.spring.footstone.AbstractComponent;
import org.github.spring.javabean.annotation.Invoke;
import org.github.spring.javabean.exception.RunException;
import org.github.spring.javabean.restful.JSONMapper;
import org.github.spring.javabean.restful.XMLMapper;
import org.github.spring.support.util.CommonUtil;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * JSONArgumentResolver.
 *
 * @author JYD_XL
 */
public class InvokeArgumentResolver extends AbstractComponent implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(Invoke.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
//        try {
//            String value = parameter.getParameterAnnotation(Invoke.class).value();
//            if (CommonUtil.isEmpty(value)) {
//                HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
//                String paramValue = request.getParameter(parameter.getParameterName());
//                switch (RequestMethod.valueOf(request.getMethod())) {
//                    case POST:
//                        return CommonUtil.isEmpty(paramValue) ? this.getMapper(parameter).readValue(request.getInputStream(), parameter.getParameterType()) : this.getMapper(parameter).readValue(paramValue, parameter.getParameterType());
//                    case GET:
//                    case PUT:
//                    case HEAD:
//                    case TRACE:
//                    case DELETE:
//                    case OPTIONS:
//                        return CommonUtil.isNotEmpty(paramValue) ? this.getMapper(parameter).readValue(paramValue, parameter.getParameterType()) : null;
//                    default:
//                        return null;
//                }
//            } else {
//                return this.getMapper(parameter).readValue(webRequest.getParameter(value), parameter.getParameterType());
//            }
//        } catch (IOException e) {
//            log.error("Exception-InvokeParam:" + e.getMessage(), e);
//        }
        return null;
    }

//    /**
//     * getMapper.
//     *
//     * @param parameter MethodParameter
//     * @return MAPPER
//     */
//    private Mapper getMapper(MethodParameter parameter) {
//        switch (parameter.getParameterAnnotation(Invoke.class).content()) {
//            case XML:
//                return XMLMapper.createWebMapper();
//            case JSON:
//                return JSONMapper.createWebMapper();
//            default:
//                throw new RunException();
//        }
//    }
}