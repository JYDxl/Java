package org.github.spring.system;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * LocaleMessageSource.
 * 
 * @author JYD_XL
 */
public class LocaleMessageSource extends ReloadableResourceBundleMessageSource implements InitializingBean {
    @Override
    public void afterPropertiesSet() throws Exception {
        this.setBasename("i18n");
    }
}