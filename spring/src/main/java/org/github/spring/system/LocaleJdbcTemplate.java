package org.github.spring.system;

import org.github.spring.system.component.HikariSource;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * LocaleJdbcTemplate.
 *
 * @author JYD_XL
 */
@Component("jdbcTemplate")
public class LocaleJdbcTemplate extends JdbcTemplate implements ApplicationContextAware {
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.setDataSource(applicationContext.getBean(HikariSource.class));
    }
}