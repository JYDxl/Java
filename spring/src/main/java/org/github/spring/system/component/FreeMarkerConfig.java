package org.github.spring.system.component;

import java.io.IOException;

import org.github.spring.support.LocaleProperties;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

/**
 * FreeMarkerConfig.
 * 
 * @author JYD_XL
 */
public class FreeMarkerConfig extends FreeMarkerConfigurer {
    /**
     * SET propertiesLocation.
     *
     * @param propertiesLocation String
     * @throws IOException Exception
     */
    public void setPropertiesLocation(String propertiesLocation) throws IOException {
        this.setFreemarkerSettings(new LocaleProperties(propertiesLocation));
    }
}