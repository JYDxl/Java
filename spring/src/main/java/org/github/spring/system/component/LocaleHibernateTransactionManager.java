package org.github.spring.system.component;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by JYD_XL on 2017/5/7.
 */
public class LocaleHibernateTransactionManager extends HibernateTransactionManager {
    @Override
    @Resource(name = "myBatisDataSource")
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    @Override
    @Resource(name = "localSessionFactoryBean")
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}