package org.github.spring.system;

import org.github.spring.system.component.ApplicationContextHolder;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.context.support.ServletContextResourceLoader;

import javax.servlet.ServletContext;

/**
 * HTML文件资源加载器.
 *
 * @author JYD_XL
 * @version 1.0
 * @see org.springframework.core.io.ResourceLoader
 * @see org.springframework.core.io.DefaultResourceLoader
 * @see org.springframework.web.context.support.ServletContextResourceLoader
 * @since 1.0
 */
public class HTMLResourceLoader extends ServletContextResourceLoader implements ResourceLoader {
    /** 构造方法. */
    public HTMLResourceLoader() {
        this(ApplicationContextHolder.getApplicationContext().getServletContext());
    }

    /**
     * 构造方法.
     *
     * @param servletContext ServletContext
     */
    public HTMLResourceLoader(ServletContext servletContext) {
        super(servletContext);
    }

    @Override
    public Resource getResource(String location) {
        return this.getResourceByPath(location);
    }
}