package org.github.spring.system.component;

import java.io.IOException;
import javax.annotation.Resource;
import javax.sql.DataSource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;


import static org.github.spring.footstone.Interface.BEAN_HIBERNATE_DATASOURCE;

/**
 * LocalHibernateSessionFactoryBean.
 * <p>
 * Created by JYD_XL on 2017/5/6.
 *
 * @author JYD_XL
 * @verson 1.0
 * @see org.springframework.beans.factory.Aware
 * @see org.springframework.beans.factory.InitializingBean
 * @see org.springframework.beans.factory.FactoryBean
 * @see org.springframework.beans.factory.DisposableBean
 * @see org.springframework.context.ResourceLoaderAware
 * @since 1.0
 */
//@Component(BEAN_LOCAL_SESSION_FACTORY)
public class LocalHibernateSessionFactoryBean extends LocalSessionFactoryBean {
    @Override
    public void afterPropertiesSet() throws IOException {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        this.setConfigLocation(resourcePatternResolver.getResource("classpath:appctx/ApplicationContext-hibernate.xml"));
        super.afterPropertiesSet();
    }

    @Override
    @Resource(name = BEAN_HIBERNATE_DATASOURCE)
    public void setDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }
}