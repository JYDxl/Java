package org.github.spring.system.component;

import org.github.spring.system.annotation.QueryInterface;
import org.mybatis.spring.mapper.MapperScannerConfigurer;

/**
 * MapperScannerConfigurer.
 * <p>
 * Created by JYD_XL on 2017/5/6.
 */
public class LocaleMapperScannerConfigurer extends MapperScannerConfigurer {
    @Override
    public void afterPropertiesSet() throws Exception {
        this.setAnnotationClass(QueryInterface.class);
        super.afterPropertiesSet();
    }
}