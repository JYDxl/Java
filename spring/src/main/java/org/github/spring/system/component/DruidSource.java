package org.github.spring.system.component;

import com.alibaba.druid.pool.DruidDataSource;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * Hibernate数据源，配合POSTGRESQL使用.
 * <p>
 * Created by JYD_XL on 2017/5/10.
 */
@SuppressWarnings("serial")
public class DruidSource extends DruidDataSource implements DataSource {
    @Override
    @PreDestroy
    public void close() {
        super.close();
    }

    @Override
    @PostConstruct
    public void init() throws SQLException {
        super.init();
    }
}