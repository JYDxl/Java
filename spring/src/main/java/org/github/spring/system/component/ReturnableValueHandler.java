package org.github.spring.system.component;

import lombok.extern.log4j.Log4j2;
import org.github.spring.footstone.AbstractComponent;
import org.github.spring.javabean.restful.Returnable;
import org.github.spring.javabean.restful.json.JSONBasicReturn;
import org.springframework.core.MethodParameter;
import org.springframework.util.Assert;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ReturnableValueHandler.
 *
 * @author JYD_XL
 */
@Log4j2
public class ReturnableValueHandler extends AbstractComponent implements HandlerMethodReturnValueHandler {
    /**
     * INIT.
     *
     * @param returnValue  Object
     * @param returnType   MethodParameter
     * @param mavContainer ModelAndViewContainer
     * @param webRequest   NativeWebRequest
     */
    @SuppressWarnings("unchecked")
    private void init(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) {
        Returnable returnableValue = (Returnable) returnValue;
        this.preProcessReturnValue(returnableValue, returnType);
        mavContainer.setRequestHandled(returnableValue.terminal());
        this.doResponseCurrentRequest(returnableValue, mavContainer, webRequest);
    }

    private void preProcessReturnValue(Returnable returnValue, MethodParameter returnType) {
        try {
            Assert.notNull(returnValue, "return null!");
            log.debug("return: ( {} ) FROM [ {}.{} ] ====> {}", returnValue.returntype(), returnType.getContainingClass().getSimpleName(), returnType.getMethod().getName(), returnValue.get());
        } catch (RuntimeException e) {
            log.error("Exception-return: " + e.getMessage(), e);
            returnValue = JSONBasicReturn.error();
            log.error("return: [ ERROR ] ====> {}", returnValue.returntype().get());
        }
    }

    private void doResponseCurrentRequest(Returnable returnValue, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) {
        if (returnValue.terminal()) {
            try {
                returnValue.collect(webRequest.getNativeRequest(HttpServletRequest.class), webRequest.getNativeResponse(HttpServletResponse.class));
            } catch (IOException e) {
                log.error("Exception-TXT/JSON/JSONP/XML/HTML:" + e.getMessage(), e);
            }
        } else {
            mavContainer.setViewName(returnValue.returntype().get() + returnValue.get());
        }
    }

    @Override
    public boolean supportsReturnType(MethodParameter returnType) {
        return Returnable.class.isAssignableFrom(returnType.getParameterType());
    }

    @Override
    public void handleReturnValue(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) {
        this.init(returnValue, returnType, mavContainer, webRequest);
    }
}