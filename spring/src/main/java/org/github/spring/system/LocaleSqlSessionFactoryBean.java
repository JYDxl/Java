package org.github.spring.system;

import org.apache.ibatis.plugin.Interceptor;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * LocaleSqlSessionFactoryBean.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.util.EventListener
 * @see org.mybatis.spring.SqlSessionFactoryBean
 * @see org.springframework.beans.factory.FactoryBean
 * @see org.springframework.context.ApplicationListener
 * @see org.springframework.beans.factory.InitializingBean
 * @since 1.0
 */
public class LocaleSqlSessionFactoryBean extends SqlSessionFactoryBean {
    @Override
    @Autowired
    public void setPlugins(Interceptor[] plugins) {
        super.setPlugins(plugins);
    }
}