package org.github.spring.system.component;

import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletContext;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * 应用上下文存储容器.
 *
 * @author JYD_XL
 * @version 1.0
 * @see org.springframework.beans.factory.Aware
 * @see org.springframework.context.ApplicationContextAware
 */
@Component
public class ApplicationContextHolder implements ApplicationContextAware {
    /** ApplicationContext. */
    private static WebApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationContextHolder.applicationContext = (WebApplicationContext) applicationContext;
    }

    /**
     * getApplicationContext.
     *
     * @return WebApplicationContext
     */
    public static WebApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * GET Bean.
     *
     * @param clazz Class<T>
     * @return T
     */
    public static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    public static <T> Map<String, T> getBeans(Class<T> clazz) {
        return applicationContext.getBeansOfType(clazz);
    }

    public static <T> Map<String, T> getBeans(Class<T> clazz, boolean includeNonSingletons, boolean allowEagerInit) {
        return applicationContext.getBeansOfType(clazz, includeNonSingletons, allowEagerInit);
    }

    /**
     * GET Bean.
     *
     * @param clazz Class<T>
     * @return T
     */
    public static <T> T getBean(String name, Class<T> clazz) {
        return applicationContext.getBean(name, clazz);
    }

    public static <T> String[] getBeanNamesForType(Class<T> clazz) {
        return applicationContext.getBeanNamesForType(clazz);
    }

    public static <T> String[] getBeanNamesForType(Class<T> clazz, boolean includeNonSingletons, boolean allowEagerInit) {
        return applicationContext.getBeanNamesForType(clazz, includeNonSingletons, allowEagerInit);
    }

    public static Resource getResource(String location) {
        return applicationContext.getResource(location);
    }

    public static Resource[] getResources(String locationPattern) throws IOException {
        return applicationContext.getResources(locationPattern);
    }

    public static ServletContext getServletContext() {
        return applicationContext.getServletContext();
    }
}