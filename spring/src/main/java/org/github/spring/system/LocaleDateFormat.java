package org.github.spring.system;

import org.github.spring.footstone.Entity;
import org.github.spring.javabean.exception.InvalidParamsException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * LocaleSimpleDateFormat.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.lang.Cloneable
 * @see java.text.DateFormat
 * @see java.text.Format
 * @see java.text.SimpleDateFormat
 */
@SuppressWarnings("serial")
public class LocaleDateFormat extends SimpleDateFormat {
    /** 构造方法. */
    public LocaleDateFormat() {
        this(Entity.DEFAULT_DATE_FORMAT);
    }

    public static ThreadLocal<DateFormat> DATE_FORMAT_THREAD_LOCAL = ThreadLocal.withInitial(LocaleDateFormat::new);

    /**
     * 构造方法.
     *
     * @param time 日期格式化形式
     */
    public LocaleDateFormat(String time) {
        super(time);
    }

    /**
     * Parsing.
     *
     * @param source String
     * @return Date
     */
    public Date parsing(String source) {
        try {
            return this.parse(source);
        } catch (ParseException e) {
            throw new InvalidParamsException(e.getMessage(), e);
        }
    }
}