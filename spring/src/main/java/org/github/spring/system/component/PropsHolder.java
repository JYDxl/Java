package org.github.spring.system.component;

import org.github.spring.footstone.Entity;
import org.github.spring.javabean.location.PropertyNameLocation;
import org.github.spring.support.util.CommonUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.function.Function;

/**
 * 配置信息存储容器.
 *
 * @author JYD_XL
 */
@Deprecated
@Component
public class PropsHolder extends PropertyPlaceholderConfigurer implements InitializingBean,
        Function<PropertyNameLocation, String> {
    /** props. */
    private Properties props;

    /** cache. */
    private final Map<String, String> cache = new HashMap<>();

    @Override
    public String apply(PropertyNameLocation name) {
        return cache.get(name.get());
    }

    protected String getValue(String name) {
        return cache.get(name);
    }

    public Properties getProps() {
        return props;
    }

    protected Map<String, String> getValues() {
        return cache;
    }

    @Override
    @Deprecated
    public void setLocations(Resource... locations) {
        super.setLocations(locations);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        this.setFileEncoding(Entity.DEFAULT_ENCODING);
        this.setLocation(new PathMatchingResourcePatternResolver().getResource(Entity.DEFAULT_PROFILE_LOCATION));
        this.props = this.mergeProperties();
        props.stringPropertyNames().forEach(each -> this.cleanData(each, props, cache));
    }

    /**
     * 配置属性信息清洗并转化.
     *
     * @param name  属性名称
     * @param props 属性信息
     * @param cache 缓存信息
     */
    private void cleanData(String name, Properties props, Map<String, String> cache) {
        //属性名称对应的值.
        String value = props.getProperty(name);
        if (value.contains(this.placeholderPrefix) && value.contains(this.placeholderSuffix)) {
            //取出第一个或者最后一个内包含属性的名称.
            String innerName = value.split("}")[0].split("\\$\\{")[1];
            //获取该属性对应的值.
            String innerValue = props.getProperty(innerName);
            if (CommonUtil.isEmpty(innerName)) {
                throw new RuntimeException(" propertyName [ " + innerName + " ] 不存在的……");
            } else {
                props.setProperty(name, value.replace("${" + innerName + "}", innerValue));
                this.cleanData(name, props, cache);
            }
        } else {
            cache.put(name, props.getProperty(name));
        }
    }
}