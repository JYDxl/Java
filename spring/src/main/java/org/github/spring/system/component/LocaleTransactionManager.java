package org.github.spring.system.component;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;

/**
 * LocaleTransactionManager.
 * <p>
 * Created by JYD_XL on 2017/5/6.
 */
@SuppressWarnings("serial")
public class LocaleTransactionManager extends DataSourceTransactionManager {}