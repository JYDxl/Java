package org.github.spring.system.resolver;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/** Created by JYD_XL on 5/28/17. */

/**
 * Abstract view resolver,support JSP and HTML.
 *
 * @author JYD_XL
 * @version 1.0
 * @see org.springframework.core.Ordered
 * @see org.springframework.beans.factory.Aware
 * @see org.springframework.web.servlet.ViewResolver
 * @see org.springframework.beans.factory.InitializingBean
 * @see org.springframework.web.context.ServletContextAware
 * @see org.springframework.context.ApplicationContextAware
 * @see org.springframework.web.servlet.view.UrlBasedViewResolver
 * @see org.springframework.context.support.ApplicationObjectSupport
 * @see org.springframework.web.servlet.view.AbstractCachingViewResolver
 * @see org.springframework.web.servlet.view.InternalResourceViewResolver
 * @see org.springframework.web.context.support.WebApplicationObjectSupport
 */
public abstract class AbstractViewResolver extends InternalResourceViewResolver implements InitializingBean {
    /** suffix of jsp. */
    public static final String SUFFIX_JSP = ".jsp";

    /** suffix of html. */
    public static final String SUFFIX_HTML = ".html";

    /** default prefix. */
    public static final String DEFAULT_PREFIX = "/WEB-INF/views/";

    /** default view name of JSP. */
    public static final String JSP_VIEW_NAMES = "jsp/**";

    /** default view name of HTML. */
    public static final String HTML_VIEW_NAMES = "html/**";

    /** default contentType of view. */
    public static final String DEFAULT_CONTENT_TYPE = "text/html;charset=utf-8";
}