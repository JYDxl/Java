package org.github.spring.system.resolver;

import org.github.spring.system.servlet.WebServlet;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * MultipartResolver.
 *
 * @author JYD_XL
 */
@Component(WebServlet.MULTIPART_RESOLVER_BEAN_NAME)
public class MultipartResolver extends CommonsMultipartResolver {}