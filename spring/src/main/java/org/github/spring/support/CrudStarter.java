package org.github.spring.support;

import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.github.spring.javabean.annotation.Column;
import org.github.spring.javabean.enumeration.Flag;
import org.github.spring.javabean.exception.DataException;
import org.github.spring.javabean.exception.RunException;
import org.github.spring.support.util.AssertUtil;
import org.github.spring.support.util.CommonUtil;


import static org.github.spring.javabean.enumeration.Flag.Between_Head;
import static org.github.spring.javabean.enumeration.Flag.Between_Tail;
import static org.github.spring.javabean.enumeration.Flag.Like;
import static org.github.spring.javabean.enumeration.Flag.NotBetween_Head;
import static org.github.spring.javabean.enumeration.Flag.NotBetween_Tail;

/**
 * CrudStarter.
 *
 * @author JYD_XL
 */
public abstract class CrudStarter {
    /** MethodDescription----AND. */
    private static final String and = "and";

    /** MethodDescription----GreaterThanOrEqualTo. */
    private static final String greaterThanOrEqualTo = "GreaterThanOrEqualTo";

    /** MethodDescription----GreaterThan. */
    private static final String greaterThan = "GreaterThan";

    /** MethodDescription----LessThanOrEqualTo. */
    private static final String lessThanOrEqualTo = "LessThanOrEqualTo";

    /** MethodDescription----LessThan. */
    private static final String lessThan = "LessThan";

    /** MethodDescription----Between. */
    private static final String betweens = "Between";

    /** MethodDescription----NotBetween. */
    private static final String notbetweens = "NotBetween";

    /** like. */
    private static final String likes = "%";

    /** Flag----Head. */
    private static final String heads = "Head";

    /** Flag----Tail. */
    private static final String tails = "Tail";

    /** Flag----UnderLine. */
    private static final String underline = "_";

    /** SPECIAL_TAG_SET. */
    private static final List<Flag> TAG = Arrays.asList(Between_Head, Between_Tail, NotBetween_Head, NotBetween_Tail);


    /**
     * Start CRUD.
     *
     * @param condModel Object
     * @param criteria  Object
     */
    public static void startCrud(Object condModel, Object criteria) {
        start(condModel, criteria, null);
    }

    /**
     * Start CRUDIgnore.
     *
     * @param condModel Object
     * @param criteria  Object
     * @param ignore    String...
     */
    @Deprecated
    public static void startIgnore(Object condModel, Object criteria, String... ignore) {
        start(condModel, criteria, Status.ignore, ignore);
    }

    /**
     * Start CRUDTarget.
     *
     * @param condModel Object
     * @param criteria  Object
     * @param target    String...
     */
    @Deprecated
    public static void startTarget(Object condModel, Object criteria, String... target) {
        start(condModel, criteria, Status.target, target);
    }

    /**
     * start.
     *
     * @param condModel Object
     * @param criteria  Object
     * @param status    Status
     * @param param     String...
     */
    private static void start(Object condModel, Object criteria, Status status, String... param) {
        AssertUtil.notNull(criteria);
        AssertUtil.notNull(condModel);
        try {
            turn(condModel, criteria, status, param);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | IntrospectionException | DataException e) {
            throw new RunException("CRUDTarget-Exception:" + e.getMessage(), e);
        }
    }

    /**
     * CURD core function.
     *
     * @param condModel Object
     * @param criteria  Object
     * @param status    Status
     * @throws IntrospectionException    Exception
     * @throws IllegalAccessException    Exception
     * @throws IllegalArgumentException  Exception
     * @throws InvocationTargetException Exception
     * @throws NoSuchMethodException     Exception
     * @throws SecurityException         Exception
     * @throws DataException             Exception
     */
    private static void turn(Object condModel, Object criteria, Status status, String... param) throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, DataException {
        // GET criteriaClass.
        Class<?> criteriaClass = criteria.getClass();
        // GET condModelClass.
        Class<?> condModelClass = condModel.getClass();
        // GET fields.
        List<Field> fields = new ArrayList<>(Arrays.asList(condModelClass.getDeclaredFields()));
        // GET PARAMS.
        List<String> params = Arrays.asList(param);

        getFields(condModelClass, fields);

        // clean.
        clear(fields, params, status);
        // INIT.
        Map<String, Between> betweenMap = new HashMap<>();

        // start.
        for (Field field : fields) {
            // 设置属性访问权限.
            field.setAccessible(true);
            //获取属性值.
            Object value = field.get(condModel);
            if (CommonUtil.isEmpty(value) || (value instanceof Collection<?> && ((Collection<?>) value).isEmpty())) {
                // 跳过空属性和空的容器.
                continue;
            } else if (value instanceof String) {
                // 去除字符串左右空格.
                value = value.toString().trim();
            }

            // GET type.
            Flag flag = field.getAnnotation(Column.class).type();
            // GET goal.
            String goal = field.getAnnotation(Column.class).goal().trim();

            // is between?
            if (TAG.contains(flag)) {
                if (betweenMap.containsKey(goal)) {
                    // SET.
                    betweenMap.get(goal).setValue(flag, value);
                } else {
                    // NEW.
                    betweenMap.put(goal, new Between(flag, value));
                }
            } else {
                // startInvoke，target first，field second.
                switch (flag) {
                    case IsNull:
                    case IsNotNull:
                        invokeMethod(criteriaClass, mark(field, goal), flag.toString(), criteria);
                        break;
                    case EqualTo:
                    case NotEqualTo:
                    case GreaterThan:
                    case GreaterThanOrEqualTo:
                    case LessThan:
                    case LessThanOrEqualTo:
                    case In:
                    case NotIn:
                        invokeMethod(criteriaClass, mark(field, goal), flag.toString(), criteria, value);
                        break;
                    case Like:
                    case NotLike:
                        invokeMethod(criteriaClass, mark(field, goal), flag.toString(), criteria, valueLikeNormal(value));
                        break;
                    case LikeBehind:
                        invokeMethod(criteriaClass, mark(field, goal), Like.toString(), criteria, valueLikeBehind(value));
                        break;
                    default:
                        break;
                }
            }
        }

        // start between.
        for (Map.Entry<String, Between> temp : betweenMap.entrySet()) {
            invokeBetween(temp.getKey(), temp.getValue(), criteriaClass, criteria);
        }
    }

    /**
     * 通过递归获取查询数据模型类的所有属性信息.
     *
     * @param condModelClass 数据模型类类型
     * @param fields         属性信息存储容器
     */
    private static void getFields(Class<?> condModelClass, List<Field> fields) {
        condModelClass = condModelClass.getSuperclass();
        if (condModelClass.isAssignableFrom(Object.class)) {return;}
        fields.addAll(Arrays.asList(condModelClass.getDeclaredFields()));
        getFields(condModelClass, fields);
    }

    /**
     * Method Invoke.
     *
     * @param criteriaClass Class<?>
     * @param target        String
     * @param detail        String
     * @param criteria      Object
     * @param value         Object...
     * @throws IllegalAccessException    Exception
     * @throws IllegalArgumentException  Exception
     * @throws InvocationTargetException Exception
     * @throws NoSuchMethodException     Exception
     * @throws SecurityException         Exception
     */
    private static void invokeMethod(Class<?> criteriaClass, String target, String detail, Object criteria, Object... value) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        if (value.length == 0) {
            criteriaClass.getDeclaredMethod(and + headUp(target) + detail).invoke(criteria);
        } else if (value.length == 1) {
            // invoke list.
            if (Collection.class.isAssignableFrom(value[0].getClass())) {
                criteriaClass.getDeclaredMethod(and + headUp(target) + detail, List.class).invoke(criteria, value[0]);
            } else {
                criteriaClass.getDeclaredMethod(and + headUp(target) + detail, value[0].getClass()).invoke(criteria, value[0]);
            }
        } else {
            //
        }
    }

    /**
     * Between Invoke.
     *
     * @param target        String
     * @param between       Between
     * @param criteriaClass Class<?>
     * @param criteria      Object
     * @throws IllegalAccessException    Exception
     * @throws IllegalArgumentException  Exception
     * @throws InvocationTargetException Exception
     * @throws NoSuchMethodException     Exception
     * @throws SecurityException         Exception
     */
    private static void invokeBetween(String target, Between between, Class<?> criteriaClass, Object criteria) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        // GET Type.
        switch (between.getType()) {
            case head:
                criteriaClass.getDeclaredMethod(and + headUp(target) + (between.method.equals(betweens) ? greaterThanOrEqualTo : lessThan), between.clazz).invoke(criteria, between.head);
                break;
            case tail:
                criteriaClass.getDeclaredMethod(and + headUp(target) + (between.method.equals(notbetweens) ? greaterThan : lessThanOrEqualTo), between.clazz).invoke(criteria, between.tail);
                break;
            case both:
                criteriaClass.getDeclaredMethod(and + headUp(target) + between.method, between.clazz, between.clazz).invoke(criteria, between.head, between.tail);
                break;
            default:
                //
                break;
        }
    }

    /**
     * GET target.
     *
     * @param field Field
     * @param goal  String
     * @return String
     */
    private static String mark(Field field, String goal) {
        return CommonUtil.isEmpty(goal) ? field.getName() : goal;
    }

    /**
     * FieldName headUp.
     *
     * @param name String
     * @return String
     */
    private static String headUp(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    /**
     * LikeNormal.
     *
     * @param value Object
     * @return String
     */
    private static String valueLikeNormal(Object value) {
        return likes + value + likes;
    }

    /**
     * LikeBehind.
     *
     * @param value Object
     * @return String
     */
    private static String valueLikeBehind(Object value) {
        return value + likes;
    }

    /**
     * Field clean.
     *
     * @param fields List
     * @param params List
     * @param status Status
     * @throws DataException Exception
     */
    private static void clear(List<Field> fields, List<String> params, Status status) throws DataException {
        // clean.
        fields.removeIf(field -> !field.isAnnotationPresent(Column.class));
        // default.
        if (status == null) {
            return;
        }
        // check.
        if (CommonUtil.isEmpty(params)) {
            throw new DataException("When the status is not null,goal can not be null!");
        }
        // PRE.
        List<String> param = params.parallelStream().filter(CommonUtil::isNotEmpty).distinct().map(String::trim).collect(Collectors.toList());
        // clean.
        switch (status) {
            case ignore:
                fields.removeIf(field -> param.contains(field.getName()) || param.contains(field.getAnnotation(Column.class).goal()));
                break;
            case target:
                fields.removeIf(field -> !param.contains(field.getName()) && !param.contains(field.getAnnotation(Column.class).goal()));
                break;
            default:
                //
                break;
        }
    }

    /**
     * Between.
     *
     * @author JYD_XL
     */
    private static class Between {
        /** method. */
        String method;

        /** head. */
        Object head;

        /** tail. */
        Object tail;

        /** class. */
        Class<?> clazz;

        /**
         * Constructor.
         *
         * @param flag  Flag
         * @param value Object
         * @throws DataException Exception
         */
        Between(Flag flag, Object value) throws DataException {
            // INIT.
            clazz = value.getClass();
            setValue(flag, value);
        }

        /**
         * GET Method name.
         *
         * @param flag Flag
         * @return String
         */
        String getMethod(Flag flag) {
            return flag.toString().split(underline)[0];
        }

        /**
         * GET Field name.
         *
         * @param flag Flag
         * @return String
         */
        String getDetail(Flag flag) {
            return flag.toString().split(underline)[1];
        }

        /**
         * SET value.
         *
         * @param flag  Flag
         * @param value Object
         * @throws DataException Exception
         */
        void setValue(Flag flag, Object value) throws DataException {
            if (method == null) {
                // INIT.
                method = getMethod(flag);
            } else if (!method.equals(getMethod(flag))) {
                throw new DataException("Between and NotBetween，use one of them!");
            } else if (!value.getClass().equals(clazz)) {
                throw new DataException("Class must be same when use between!");
            } else {
                //
            }

            // INIT.
            String type = getDetail(flag);
            if (type.equals(heads)) {
                head = value;
            } else if (type.equals(tails)) {
                tail = value;
            } else {
                //
            }
        }

        /**
         * GET status.
         *
         * @return Status
         */
        Status getType() {
            if (head != null && tail == null) {
                return Status.head;
            } else if (head == null && tail != null) {
                return Status.tail;
            } else {
                return Status.both;
            }
        }
    }

    /**
     * status.
     *
     * @author JYD_XL
     */
    private enum Status {
        head, tail, both, target, ignore
    }
}