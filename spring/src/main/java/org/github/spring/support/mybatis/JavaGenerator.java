package org.github.spring.support.mybatis;

import java.util.List;

import org.mybatis.generator.api.dom.java.CompilationUnit;
import org.mybatis.generator.codegen.AbstractJavaClientGenerator;
import org.mybatis.generator.codegen.AbstractXmlGenerator;

/**
 * JavaGenerator.
 * 
 * @author JYD_XL
 */
public class JavaGenerator extends AbstractJavaClientGenerator {
    /**
     * Constructor.
     *
     * @param requiresXMLGenerator boolean
     */
    public JavaGenerator(boolean requiresXMLGenerator) {
        super(requiresXMLGenerator);
    }

    @Override
    public AbstractXmlGenerator getMatchedXMLGenerator() {
        return null;
    }

    @Override
    public List<CompilationUnit> getCompilationUnits() {
        return null;
    }
}