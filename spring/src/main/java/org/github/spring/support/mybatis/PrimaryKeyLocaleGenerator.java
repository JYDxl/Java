package org.github.spring.support.mybatis;

import java.util.List;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.ProgressCallback;
import org.mybatis.generator.api.dom.java.CompilationUnit;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.codegen.mybatis3.model.PrimaryKeyGenerator;
import org.mybatis.generator.config.Context;

/**
 * PrimaryKeyLocaleGenerator.
 * 
 * @author JYD_XL
 */
public class PrimaryKeyLocaleGenerator extends PrimaryKeyGenerator {
    @Override
    public List<CompilationUnit> getCompilationUnits() {
        return super.getCompilationUnits();
    }

    @Override
    public String getRootClass() {
        return super.getRootClass();
    }

    @Override
    protected void addDefaultConstructor(TopLevelClass topLevelClass) {
        super.addDefaultConstructor(topLevelClass);
    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public void setContext(Context context) {
        super.setContext(context);
    }

    @Override
    public IntrospectedTable getIntrospectedTable() {
        return super.getIntrospectedTable();
    }

    @Override
    public void setIntrospectedTable(IntrospectedTable introspectedTable) {
        super.setIntrospectedTable(introspectedTable);
    }

    @Override
    public List<String> getWarnings() {
        return super.getWarnings();
    }

    @Override
    public void setWarnings(List<String> warnings) {
        super.setWarnings(warnings);
    }

    @Override
    public ProgressCallback getProgressCallback() {
        return super.getProgressCallback();
    }

    @Override
    public void setProgressCallback(ProgressCallback progressCallback) {
        super.setProgressCallback(progressCallback);
    }
}