package org.github.spring.support.mybatis;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.github.spring.support.util.CommonUtil;
import org.github.spring.support.util.StringUtil;
import org.github.spring.system.LocaleDateFormat;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.CompilationUnit;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.InnerClass;
import org.mybatis.generator.api.dom.java.InnerEnum;
import org.mybatis.generator.api.dom.java.JavaElement;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.internal.DefaultCommentGenerator;

/**
 * CommentGenerator.
 *
 * @author JYD_XL
 * @version 1.0
 * @since 1.0
 */
public class CommentGenerator extends DefaultCommentGenerator {
    private static final String SPACE = " ";

    @Override
    public void addModelClassComment(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        topLevelClass.addJavaDocLine("/**");
        String shortName = topLevelClass.getType().getShortNameWithoutTypeArguments();
        String tableName = introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime();
        topLevelClass.addJavaDocLine(StringUtil.joinWith(SPACE, " *", shortName, "[", tableName, "]."));
        topLevelClass.addJavaDocLine(" *");
        topLevelClass.addJavaDocLine(" * @author JYD_XL");
        topLevelClass.addJavaDocLine(" * @version " + new LocaleDateFormat("yyyy-MM-dd").format(new Date()));
        topLevelClass.addJavaDocLine("*/");
    }

    @Override
    public void addFieldComment(Field field, IntrospectedTable introspectedTable, IntrospectedColumn
            introspectedColumn) {
        field.addJavaDocLine(new StringBuilder().append("/** ").append(CommonUtil.isEmpty(introspectedColumn
                .getRemarks()) ? introspectedColumn.getActualColumnName() : introspectedColumn.getRemarks()).append
                ("" + "(").append(introspectedTable.getTableConfiguration().getTableName()).append('.').append
                (introspectedColumn.getActualColumnName()).append(").").append(" */").toString());
    }

    @Override
    public void addGetterComment(Method method, IntrospectedTable introspectedTable, IntrospectedColumn
            introspectedColumn) {
        method.addJavaDocLine("/**");
        method.addJavaDocLine(" * GET ".concat(CommonUtil.isEmpty(introspectedColumn.getRemarks()) ?
                introspectedColumn.getActualColumnName() : introspectedColumn.getRemarks()).concat("(").concat
                (introspectedTable.getTableConfiguration().getTableName()).concat(".").concat(introspectedColumn
                .getActualColumnName()).concat(")."));
        method.addJavaDocLine(" *");
        method.addJavaDocLine(new StringBuilder().append(" * @return ").append(introspectedColumn.getJavaProperty())
                .append(" ").append(introspectedColumn.getJdbcTypeName()).toString());
        method.addJavaDocLine(" */");
    }

    @Override
    public void addSetterComment(Method method, IntrospectedTable introspectedTable, IntrospectedColumn
            introspectedColumn) {
        method.addJavaDocLine("/**");
        method.addJavaDocLine(new StringBuilder().append(" * SET ").append(CommonUtil.isEmpty(introspectedColumn
                .getRemarks()) ? introspectedColumn.getActualColumnName() : introspectedColumn.getRemarks()).append
                ("" + "(").append(introspectedTable.getTableConfiguration().getTableName()).append('.').append
                (introspectedColumn.getActualColumnName()).append(").").toString());
        method.addJavaDocLine(" *");
        method.getParameters().forEach(param -> method.addJavaDocLine(" * @param " + param.getName() + " " +
                introspectedColumn.getJdbcTypeName()));
        method.addJavaDocLine(" */");
    }

    @Override
    public void addJavaFileComment(CompilationUnit compilationUnit) {
        compilationUnit.addFileCommentLine(StringUtil.joinWith(" ", "/**", "Created by JYD_XL ON [", new SimpleDateFormat("yyyy-MM-dd").format(new Date()), "].", "*/"));
        super.addJavaFileComment(compilationUnit);
    }

    @Override
    public void addComment(XmlElement xmlElement) {
        super.addComment(xmlElement);
    }

    @Override
    public void addRootComment(XmlElement rootElement) {
        super.addRootComment(rootElement);
    }

    @Override
    public void addConfigurationProperties(Properties properties) {
        super.addConfigurationProperties(properties);
    }

    @Override
    protected void addJavadocTag(JavaElement javaElement, boolean markAsDoNotDelete) {
        super.addJavadocTag(javaElement, markAsDoNotDelete);
    }

    @Override
    protected String getDateString() {
        return super.getDateString();
    }

    @Override
    public void addClassComment(InnerClass innerClass, IntrospectedTable introspectedTable) {
        super.addClassComment(innerClass, introspectedTable);
    }

    @Override
    public void addEnumComment(InnerEnum innerEnum, IntrospectedTable introspectedTable) {
        super.addEnumComment(innerEnum, introspectedTable);
    }

    @Override
    public void addFieldComment(Field field, IntrospectedTable introspectedTable) {
        super.addFieldComment(field, introspectedTable);
    }

    @Override
    public void addGeneralMethodComment(Method method, IntrospectedTable introspectedTable) {
        super.addGeneralMethodComment(method, introspectedTable);
    }

    @Override
    public void addClassComment(InnerClass innerClass, IntrospectedTable introspectedTable, boolean markAsDoNotDelete) {
        super.addClassComment(innerClass, introspectedTable, markAsDoNotDelete);
    }
}