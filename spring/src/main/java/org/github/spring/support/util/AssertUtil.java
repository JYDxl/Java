package org.github.spring.support.util;

import org.github.spring.javabean.exception.InvalidParamsException;
import org.springframework.util.Assert;

import java.util.Collection;

import static java.lang.String.format;

/**
 * AssertUtil.
 *
 * @author JYD_XL
 */
public abstract class AssertUtil extends Assert {
    /** 参数不能为空. */
    private static final String NOT_NULL = "参数【 %s 】不能为空!";

    /**
     * 参数不能为Null.
     *
     * @param parameter Object
     * @param fieldName String
     */
    public static void notNull(Object parameter, String fieldName) {
        if (parameter == null) { throw new InvalidParamsException(format(NOT_NULL, fieldName)); }
    }

    /**
     * 参数不能为空.
     *
     * @param parameter Object
     * @param fieldName String
     */
    public static void notEmpty(Object parameter, String fieldName) {
        //TODO 对parameter进行分类处理，最后统一抛出异常信息.
        if (CommonUtil.isEmpty(parameter)) { throw new InvalidParamsException(format(NOT_NULL, fieldName)); }
    }

    /**
     * notNull.
     *
     * @param obj Object
     */
    public static void notNull(Object obj) {
        notNull(obj, "invalid argument!");
    }

    /**
     * notNull.
     *
     * @param collection Collection
     * @param message    String
     */
    public static <T> void notNull(Collection<T> collection, String message) {
        if (CommonUtil.isEmpty(collection)) { throw new InvalidParamsException(message); }
    }

    /**
     * notNull.
     *
     * @param collection Collection
     */
    public static <T> void notNull(Collection<T> collection) {
        notNull(collection, "invalid argument!");
    }

    /**
     * paramIn.
     *
     * @param collection Collection
     * @param param      T
     * @return boolean
     */
    public static <T> boolean paramIn(Collection<T> collection, T param) {
        AssertUtil.notNull(collection);
        return collection.contains(param);
    }
}