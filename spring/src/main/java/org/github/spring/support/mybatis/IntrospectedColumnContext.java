package org.github.spring.support.mybatis;

import org.mybatis.generator.api.IntrospectedColumn;

/**
 * IntrospectedColumnPlugin.
 * 
 * @author JYD_XL
 */
public class IntrospectedColumnContext extends IntrospectedColumn {}