package org.github.spring.support.mybatis;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.plugins.RenameExampleClassPlugin;

import java.util.List;

/**
 * Created by JYD_XL on 2017/5/18.
 */
public class RenameClassPlugin extends RenameExampleClassPlugin {
    @Override
    public boolean validate(List<String> warnings) {
        return warnings.isEmpty();
    }

    @Override
    public void initialized(IntrospectedTable introspectedTable) {
        String oldKey = introspectedTable.getPrimaryKeyType();
        String oldExample = introspectedTable.getExampleType();
        String oldMapper = introspectedTable.getMyBatis3JavaMapperType();
        String oldXML = introspectedTable.getMyBatis3XmlMapperFileName();
        introspectedTable.setPrimaryKeyType(oldKey.replace("EntityKey", "Key"));
        introspectedTable.setExampleType(oldExample.replace("EntityExample", "Example"));
        introspectedTable.setMyBatis3JavaMapperType(oldMapper.replace("EntityMapper", "Mapper"));
        introspectedTable.setMyBatis3XmlMapperFileName(oldXML.replace("EntityMapper", "SQLMapper"));
    }
}