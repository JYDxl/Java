package org.github.spring.support.factory;

import java.util.HashSet;
import java.util.Set;

import org.springframework.format.AnnotationFormatterFactory;
import org.springframework.format.datetime.DateTimeFormatAnnotationFormatterFactory;
import org.springframework.format.datetime.joda.JodaDateTimeFormatAnnotationFormatterFactory;
import org.springframework.format.datetime.standard.Jsr310DateTimeFormatAnnotationFormatterFactory;
import org.springframework.format.number.NumberFormatAnnotationFormatterFactory;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.stereotype.Component;

/**
 * LocaleConversionServiceFactoryBean.
 * 
 * @author JYD_XL
 */
@Component
public class LocaleConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {
    @Override
    public void afterPropertiesSet() {
        Set<AnnotationFormatterFactory<?>> formatterFactorySet = new HashSet<>();
        formatterFactorySet.add(new DateTimeFormatAnnotationFormatterFactory());
        formatterFactorySet.add(new JodaDateTimeFormatAnnotationFormatterFactory());
        formatterFactorySet.add(new Jsr310DateTimeFormatAnnotationFormatterFactory());
        formatterFactorySet.add(new NumberFormatAnnotationFormatterFactory());
        this.setFormatters(formatterFactorySet);
        super.afterPropertiesSet();
    }
}