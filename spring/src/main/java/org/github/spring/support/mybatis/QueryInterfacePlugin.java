package org.github.spring.support.mybatis;

import java.util.List;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;

/**
 * SqlMapperXMLOverridePlugin.
 *
 * @author JYD_XL
 */
public class QueryInterfacePlugin extends PluginAdapter {
    private static final String QUERY_INTERFACE_ANNOTATION = "@QueryInterface";

    private static final FullyQualifiedJavaType QUERY_INTERFACE_ANNOTATION_TYPE = new FullyQualifiedJavaType("org.github.spring.system.annotation.QueryInterface");

    @Override
    public boolean validate(List<String> warnings) {
        return warnings.isEmpty();
    }

    @Override
    public boolean clientGenerated(Interface anInterface, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        addQueryInterfaceAnnotation(anInterface);
        return super.clientGenerated(anInterface, topLevelClass, introspectedTable);
    }

    private void addQueryInterfaceAnnotation(Interface anInterface) {
        anInterface.addAnnotation(QUERY_INTERFACE_ANNOTATION);
        anInterface.addImportedType(QUERY_INTERFACE_ANNOTATION_TYPE);
    }
}