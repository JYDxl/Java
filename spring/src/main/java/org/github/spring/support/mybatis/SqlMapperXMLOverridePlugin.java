package org.github.spring.support.mybatis;

import org.github.spring.javabean.exception.RunException;
import org.mybatis.generator.api.GeneratedXmlFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;

import java.lang.reflect.Field;
import java.util.List;

/**
 * SqlMapperXMLOverridePlugin.
 *
 * @author JYD_XL
 */
public class SqlMapperXMLOverridePlugin extends PluginAdapter {
    @Override
    public boolean validate(List<String> warnings) {
        return warnings.isEmpty();
    }

    @Override
    public boolean sqlMapGenerated(GeneratedXmlFile generatedXmlFile, IntrospectedTable introspectedTable) {
        try {
            Field isMergeable = generatedXmlFile.getClass().getDeclaredField("isMergeable");
            isMergeable.setAccessible(true);
            isMergeable.setBoolean(generatedXmlFile, false);
            return super.sqlMapGenerated(generatedXmlFile, introspectedTable);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new RunException("SqlMapperXMLOverriding-Exception: " + e.getMessage(), e);
        }
    }
}