/** Created by JYD_XL on 2017/5/25. */
package org.github.spring.support.util;

import org.apache.commons.lang3.StringUtils;

public abstract class StringUtil extends StringUtils {
    public static boolean isBlank(Object str) {
        return str == null || "".equals(str.toString().trim());
    }
}