package org.github.spring.support;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.function.Supplier;

/**
 * LocaleProperties.
 *
 * @author JYD_XL
 */
@SuppressWarnings("serial")
public class LocaleProperties extends Properties {
    /** Constructor. */
    public LocaleProperties() {}

    /**
     * Constructor.
     *
     * @param properties Properties
     */
    public LocaleProperties(Properties properties) {
        super(properties);
    }

    /**
     * Constructor.
     *
     * @param loader   ResourceLoader
     * @param location String
     * @throws IOException Exception
     */
    public LocaleProperties(Supplier<ResourceLoader> loader, String location) throws IOException {
        this.load(loader.get().getResource(location).getInputStream());
    }

    /**
     * Constructor.
     *
     * @param location String
     * @throws IOException Exception
     */
    public LocaleProperties(String location) throws IOException {
        this(PathMatchingResourcePatternResolver::new, location);
    }

    /**
     * Constructor.
     *
     * @param resource Resource
     */
    public LocaleProperties(Resource resource) throws IOException {
        this.load(resource.getInputStream());
    }

    /**
     * Constructor.
     *
     * @param inputStream InputStream
     */
    public LocaleProperties(InputStream inputStream) throws IOException {
        this.load(inputStream);
    }
}