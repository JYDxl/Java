package org.github.spring.javabean.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Invoke.
 * 
 * @author JYD_XL
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER })
@Documented
public @interface Invoke {
    /**
     * type.
     * 
     * @return Type
     */
    Type content() default Type.JSON;

    /**
     * value.
     * 
     * @return String
     */
    String value() default "";

    /**
     * Type.
     * 
     * @author JYD_XL
     */
    enum Type {
        JSON, XML
    }
}