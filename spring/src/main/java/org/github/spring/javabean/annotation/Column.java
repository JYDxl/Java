package org.github.spring.javabean.annotation;

import org.github.spring.javabean.enumeration.Flag;

import javax.validation.constraints.NotNull;
import java.lang.annotation.*;

import static org.github.spring.javabean.enumeration.Flag.EqualTo;

/**
 * 增删改查条件注入标记注解.
 *
 * @author JYD_XL
 * @see java.lang.annotation.Annotation
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Documented
public @interface Column {
    /**
     * GET type.
     *
     * @return type
     */
    Flag type() default EqualTo;

    /**
     * GET goal.
     *
     * @return goal
     */
    @NotNull
    String goal() default "";
}