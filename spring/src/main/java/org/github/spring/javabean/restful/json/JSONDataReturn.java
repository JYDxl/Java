package org.github.spring.javabean.restful.json;

import org.github.spring.javabean.restful.JSONReturn;

/**
 * JSONDataReturn.
 *
 * @param <T> data
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.restful.Returnable
 * @see org.github.spring.javabean.restful.JSONReturn
 * @see org.github.spring.javabean.restful.json.JSONBasicReturn
 */
@SuppressWarnings("serial")
public class JSONDataReturn<T> extends JSONBasicReturn implements JSONReturn {
    /** data. */
    private T data;

    /**
     * GET data.
     *
     * @return T
     */
    public T getData() {
        return data;
    }

    /**
     * SET data.
     *
     * @param data T
     * @return JSONDataReturn
     */
    public JSONDataReturn<T> setData(T data) {
        this.data = data;
        return this;
    }

    /** Constructor. */
    public JSONDataReturn() {}

    /**
     * Constructor.
     *
     * @param data T
     */
    public JSONDataReturn(T data) {
        this.setData(data);
    }
}