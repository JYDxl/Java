package org.github.spring.javabean.enumeration;

/**
 * CRUDFlag.
 *
 * @author JYD_XL
 */
public enum Flag {
    IsNull, IsNotNull, EqualTo, NotEqualTo, GreaterThan, GreaterThanOrEqualTo, LessThan, LessThanOrEqualTo, Like, LikeBehind, NotLike, In, NotIn, Between_Head, Between_Tail, NotBetween_Head, NotBetween_Tail
}