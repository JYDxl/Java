package org.github.spring.javabean.restful.json;

import com.github.pagehelper.Page;
import org.github.spring.javabean.restful.JSONReturn;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
 * JSONPageReturn.
 *
 * @param <E> element
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.restful.Returnable
 * @see org.github.spring.javabean.restful.JSONReturn
 * @see org.github.spring.javabean.restful.json.JSONBasicReturn
 * @see org.github.spring.javabean.restful.json.JSONArrayReturn
 * @since 1.0
 */
@SuppressWarnings("serial")
public class JSONPageReturn<E> extends JSONArrayReturn<E> implements JSONReturn {
    /** total. */
    private long total = TOTAL;

    /**
     * GET total.
     *
     * @return long
     */
    public long getTotal() {
        return this.total;
    }

    /**
     * SET total.
     *
     * @param total long
     * @return JSONPageReturn
     */
    public JSONPageReturn<E> setTotal(long total) {
        this.total = total;
        return this;
    }

    /** Constructor. */
    public JSONPageReturn() {}

    /**
     * Constructor.
     *
     * @param total long
     * @param data  Collection<? extends E>
     */
    public JSONPageReturn(long total, Collection<? extends E> data) {
        this.setTotal(total).setData(data);
    }

    /**
     * Constructor.
     *
     * @param total long
     * @param data  E[]
     */
    public JSONPageReturn(long total, E[] data) {
        this.setTotal(total).setData(data);
    }

    /**
     * Constructor.
     *
     * @param total long
     * @param data  E
     */
    public JSONPageReturn(long total, E data) {
        this.setTotal(total).setData(data);
    }

    /**
     * Constructor.
     *
     * @param total long
     * @param data  Stream<? extends E>
     */
    public JSONPageReturn(long total, Stream<? extends E> data) {
        this.setTotal(total).setData(data);
    }

    /**
     * Constructor.
     *
     * @param page Page<? extends E>
     */
    private JSONPageReturn(Page<? extends E> page) {
        this(page.getTotal(), page);
    }

    /**
     * Constructor.
     *
     * @param page List<? extends E>
     */
    @SuppressWarnings("unchecked")
    public JSONPageReturn(List<? extends E> page) {
        this((Page<? extends E>) page);
    }

    /**
     * Constructor.
     *
     * @param page Page<? super E>
     * @param data Collection<? extends E>
     */
    private JSONPageReturn(Page<? super E> page, Collection<? extends E> data) {
        this(page.getTotal(), data);
    }

    /**
     * Constructor.
     *
     * @param page Page<? super E>
     * @param data E[]
     */
    private JSONPageReturn(Page<? super E> page, E[] data) {
        this(page.getTotal(), data);
    }

    /**
     * Constructor.
     *
     * @param page Page<? super E>
     * @param data E
     */
    private JSONPageReturn(Page<? super E> page, E data) {
        this(page.getTotal(), data);
    }

    /**
     * Constructor.
     *
     * @param page Page<? super E>
     * @param data Stream<? extends E>
     */
    private JSONPageReturn(Page<? super E> page, Stream<? extends E> data) {
        this(page.getTotal(), data);
    }

    /**
     * Constructor.
     *
     * @param page List<? super E>
     * @param data Collection<? extends E>
     */
    @SuppressWarnings("unchecked")
    public JSONPageReturn(List<? super E> page, Collection<? extends E> data) {
        this((Page<? super E>) page, data);
    }

    /**
     * Constructor.
     *
     * @param page List<? super E>
     * @param data E[]
     */
    @SuppressWarnings("unchecked")
    public JSONPageReturn(List<? super E> page, E[] data) {
        this((Page<? super E>) page, data);
    }

    /**
     * Constructor.
     *
     * @param page List<? super E>
     * @param data E
     */
    @SuppressWarnings("unchecked")
    public JSONPageReturn(List<? super E> page, E data) {
        this((Page<? super E>) page, data);
    }

    /**
     * Constructor.
     *
     * @param page List<? super E>
     * @param data Stream<? extends E>
     */
    @SuppressWarnings("unchecked")
    public JSONPageReturn(List<? super E> page, Stream<? extends E> data) {
        this((Page<? super E>) page, data);
    }
}