package org.github.spring.javabean.restful;

import java.util.function.Supplier;

/**
 * Return type enumeration.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.lang.Enum
 * @see java.io.Serializable
 * @see java.lang.Comparable
 * @see java.util.function.Supplier
 */
public enum ReturnType implements Supplier<String> {
    XML("application/xml"), JSON("application/json"), PLAIN("text/plain"), JSONP("application/jsonp"), HTML("html/"), JSP("jsp/");

    /** return type. */
    private final String _type;

    /**
     * Constructor.
     *
     * @param type String
     */
    ReturnType(String type) {
        this._type = type;
    }

    @Override
    public String get() {
        return _type;
    }
}