package org.github.spring.javabean.restful.json;

import org.github.spring.javabean.restful.JSONReturn;

import java.util.Collection;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;

/**
 * JSONReturn of array.
 *
 * @param <E> element
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.restful.Returnable
 * @see org.github.spring.javabean.restful.JSONReturn
 * @see org.github.spring.javabean.restful.json.JSONBasicReturn
 */
@SuppressWarnings("serial")
public class JSONArrayReturn<E> extends JSONBasicReturn implements JSONReturn {
    /** data. */
    private Object[] data = ARRAY;

    /**
     * GET data.
     *
     * @return E[]
     */
    @SuppressWarnings("unchecked")
    public E[] getData() {
        return (E[]) data;
    }

    /**
     * SET data.
     *
     * @param data E[]
     * @return JSONArrayReturn
     */
    public JSONArrayReturn<E> setData(E[] data) {
        if (data != null) {this.data = data;}
        return this;
    }

    /**
     * SET data.
     *
     * @param data Collection<? extends E>
     * @return JSONArrayReturn
     */
    public JSONArrayReturn<E> setData(Collection<? extends E> data) {
        if (data != null) {this.data = data.toArray();}
        return this;
    }

    /**
     * SET data.
     *
     * @param data E
     * @return JSONArrayReturn
     */
    public JSONArrayReturn<E> setData(E data) {
        this.data = new Object[]{data};
        return this;
    }

    /**
     * SET data.
     *
     * @param data Stream<? extends E>
     * @return JSONArrayReturn
     */
    public JSONArrayReturn<E> setData(Stream<? extends E> data) {
        if (data != null) {this.data = data.toArray();}
        return this;
    }

    /** Constructor. */
    public JSONArrayReturn() {}

    /**
     * Constructor.
     *
     * @param data Collection<? extends E>
     */
    public JSONArrayReturn(Collection<? extends E> data) {
        this.setData(data);
    }

    /**
     * Constructor.
     *
     * @param data E[]
     */
    public JSONArrayReturn(E[] data) {
        this.setData(data);
    }

    /**
     * Constructor.
     *
     * @param data E
     */
    public JSONArrayReturn(E data) {
        this.setData(data);
    }

    /**
     * Constructor.
     *
     * @param data Stream<? extends E>
     */
    public JSONArrayReturn(Stream<? extends E> data) {
        this.setData(data);
    }

    /**
     * GET data.
     *
     * @return Collection
     */
    @SuppressWarnings("unchecked")
    public Collection<E> toCollection() {
        return (Collection<E>) asList(data);
    }

    /**
     * GET data.
     *
     * @return Stream
     */
    @SuppressWarnings("unchecked")
    public Stream<E> toStream() {
        return (Stream<E>) stream(data);
    }

    /**
     * GET data.
     *
     * @return E[]
     */
    public E[] toArray() {
        return this.getData();
    }
}