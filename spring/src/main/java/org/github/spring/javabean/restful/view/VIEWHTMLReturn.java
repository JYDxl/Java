package org.github.spring.javabean.restful.view;

import org.github.spring.javabean.restful.ReturnType;
import org.github.spring.javabean.restful.VIEWReturn;

import static org.github.spring.javabean.restful.ReturnType.HTML;

/**
 * VIEWHTMLReturn.
 *
 * @author JYD_XL
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.restful.Returnable
 * @see org.github.spring.javabean.restful.VIEWReturn
 */
@FunctionalInterface
public interface VIEWHTMLReturn extends VIEWReturn {
    @Override
    default ReturnType returntype() {
        return HTML;
    }
}