package org.github.spring.javabean.restful;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import org.github.spring.javabean.exception.HandlerException;
import org.github.spring.support.util.AssertUtil;
import org.github.spring.system.LocaleDateFormat;

import java.io.IOException;
import java.util.Collection;
import java.util.TimeZone;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.MapperFeature.SORT_PROPERTIES_ALPHABETICALLY;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_NULL_MAP_VALUES;
import static org.github.spring.footstone.Entity.DEFAULT_TIME_ZONE;

/**
 * JSONMapper.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see com.fasterxml.jackson.core.Versioned
 * @see com.fasterxml.jackson.core.TreeCodec
 * @see com.fasterxml.jackson.core.ObjectCodec
 * @see com.fasterxml.jackson.databind.ObjectMapper
 * @since 1.0
 */
@SuppressWarnings("serial")
public final class JSONMapper extends ObjectMapper {
    /** Constructor. */
    private JSONMapper() {}

    /**
     * JSONString->JAVABean.
     *
     * @param json  JSONString
     * @param clazz class
     * @return JAVABean
     */
    public <T> T toJAVABean(String json, Class<T> clazz) {
        AssertUtil.notNull(json);
        AssertUtil.notNull(clazz);
        try {
            return this.readValue(json, clazz);
        } catch (IOException e) {
            throw new HandlerException(e.getMessage(), e);
        }
    }

    /**
     * JSONString->JAVABeanCollection.
     *
     * @param json  JSONString
     * @param clazz class...
     * @return Collection
     */
    public <T> Collection<T> toCollection(String json, Class<? extends Collection<T>> collection, Class<?>... clazz) {
        AssertUtil.notNull(json);
        AssertUtil.notNull(clazz);
        try {
            return this.readValue(json, this.getTypeFactory().constructParametricType(collection, clazz));
        } catch (IOException e) {
            throw new HandlerException(e.getMessage(), e);
        }
    }

    /**
     * JAVABean->JSONString.
     *
     * @param javaBean JAVABean
     * @return JSONString
     */
    public String toJSONString(Object javaBean) {
        AssertUtil.notNull(javaBean);
        try {
            return this.writeValueAsString(javaBean);
        } catch (JsonProcessingException e) {
            throw new HandlerException(e.getMessage(), e);
        }
    }

    /**
     * GET instance.
     *
     * @return JSONMapper
     */
    public static JSONMapper createMobileMapper() {
        return Holder._mobile_instance;
    }

    /**
     * GET instance.
     *
     * @return JSONMapper
     */
    public static JSONMapper createWebMapper() {
        return Holder._web_instance;
    }

    /**
     * static inner class.
     *
     * @author JYD_XL
     */
    private static class Holder {
        /** Constructor. */
        private Holder() {}

        /** instance. */
        private static final JSONMapper _mobile_instance = new JSONMapper();

        /** instance. */
        private static final JSONMapper _web_instance = new JSONMapper();

        static {
            _web_instance.setDateFormat(ThreadLocal.withInitial(LocaleDateFormat::new).get()).setTimeZone(TimeZone.getTimeZone(DEFAULT_TIME_ZONE)).disable(WRITE_NULL_MAP_VALUES).setSerializationInclusion(NON_NULL).disable(FAIL_ON_UNKNOWN_PROPERTIES).enable(SORT_PROPERTIES_ALPHABETICALLY).registerModule(new JaxbAnnotationModule());
        }
    }
}