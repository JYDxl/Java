package org.github.spring.javabean.restful;

/**
 * VIEW TOP INTERFACE.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.restful.Returnable
 * @since 1.0
 */
@FunctionalInterface
public interface VIEWReturn extends Returnable {
    @Override
    default boolean terminal() {
        return false;
    }
}