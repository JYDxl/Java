package org.github.spring.javabean.restful.json;

import org.github.spring.javabean.restful.JSONReturn;

/**
 * JSONCountReturn.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.restful.Returnable
 * @see org.github.spring.javabean.restful.JSONReturn
 * @see org.github.spring.javabean.restful.json.JSONBasicReturn
 */
@SuppressWarnings("serial")
public class JSONCountReturn extends JSONBasicReturn implements JSONReturn {
    /** data. */
    private long data = COUNT;

    /**
     * GET data.
     *
     * @return long
     */
    public long getData() {
        return data;
    }

    /**
     * SET data.
     *
     * @param data long
     * @return JSONCountReturn
     */
    public JSONCountReturn setData(long data) {
        this.data = data;
        return this;
    }

    /** Constructor. */
    public JSONCountReturn() {}

    /**
     * Constructor.
     *
     * @param data long
     */
    public JSONCountReturn(long data) {
        this.setData(data);
    }

    /**
     * Constructor.
     *
     * @param data integer
     */
    public JSONCountReturn(int data) {
        this.setData(data);
    }
}