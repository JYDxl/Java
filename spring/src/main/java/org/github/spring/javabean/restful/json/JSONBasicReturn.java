package org.github.spring.javabean.restful.json;

import org.github.spring.javabean.restful.JSONMapper;
import org.github.spring.javabean.restful.JSONReturn;
import org.github.spring.support.util.CommonUtil;

import java.io.IOException;
import java.io.OutputStream;

/**
 * JSONBasicReturn.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.restful.Returnable
 * @see org.github.spring.javabean.restful.JSONReturn
 */
@SuppressWarnings("serial")
public class JSONBasicReturn implements JSONReturn {
    /** CODE. */
    private int retCode = RET_OK_CODE;

    /** MSG. */
    private String retMsg = RET_OK_MSG;

    /**
     * GET CODE.
     *
     * @return integer
     */
    public int getRetCode() {
        return retCode;
    }

    /**
     * SET CODE.
     *
     * @param retCode integer
     * @return JSONBasicReturn
     */
    public JSONBasicReturn setRetCode(int retCode) {
        this.retCode = retCode;
        return this;
    }

    /**
     * GET MSG.
     *
     * @return String
     */
    public String getRetMsg() {
        return retMsg;
    }

    /**
     * SET MSG.
     *
     * @param retMsg String
     * @return JSONBasicReturn
     */
    public JSONBasicReturn setRetMsg(String retMsg) {
        if (CommonUtil.isNotEmpty(retMsg)) { this.retMsg = retMsg;}
        return this;
    }

    /** Constructor. */
    public JSONBasicReturn() {}

    /**
     * Constructor.
     *
     * @param retCode integer
     * @param retMsg  String
     */
    public JSONBasicReturn(int retCode, String retMsg) {
        this.setRetCode(retCode).setRetMsg(retMsg);
    }

    @Override
    public boolean functional() {
        return false;
    }

    @Override
    public String get() {
        return this.createMapper().toJSONString(this);
    }

    @Override
    public String toString() {
        return this.get();
    }

    @Override
    public void accept(OutputStream outputStream) throws IOException {
        this.createMapper().writeValue(outputStream, this);
    }

    private JSONMapper createMapper() {
        return JSONMapper.createWebMapper();
    }

    /**
     * SYSTEM_ERROR.
     *
     * @return JSONBasicReturn
     */
    public static JSONBasicReturn error() {
        return new JSONBasicReturn(RET_ERROR_CODE, RET_ERROR_MSG);
    }
}