package org.github.spring.javabean.exception;

/**
 * HandlerException.
 * 
 * @author JYD_XL
 */
@SuppressWarnings("serial")
public class HandlerException extends RunException {
    /** Constructor. */
    public HandlerException() {}

    /**
     * Constructor.
     *
     * @param message String
     */
    public HandlerException(String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param cause Throwable
     */
    public HandlerException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message String
     * @param cause Throwable
     */
    public HandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param message String
     * @param cause Throwable
     * @param enableSuppression boolean
     * @param writableStackTrace boolean
     */
    public HandlerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}