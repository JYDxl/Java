package org.github.spring.javabean.restful;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import java.io.IOException;
import java.util.Collection;
import java.util.TimeZone;
import org.github.spring.footstone.Entity;
import org.github.spring.javabean.exception.HandlerException;
import org.github.spring.support.util.AssertUtil;
import org.github.spring.system.LocaleDateFormat;


import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.MapperFeature.SORT_PROPERTIES_ALPHABETICALLY;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_NULL_MAP_VALUES;

/**
 * XMLMapper.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see com.fasterxml.jackson.core.TreeCodec
 * @see com.fasterxml.jackson.core.Versioned
 * @see com.fasterxml.jackson.core.ObjectCodec
 * @see com.fasterxml.jackson.databind.ObjectMapper
 * @see com.fasterxml.jackson.dataformat.xml.XmlMapper
 * @since 1.0
 */
@SuppressWarnings("serial")
public class XMLMapper extends XmlMapper {
    /** Constructor. */
    private XMLMapper() {}

    /**
     * XMLString->JAVABean.
     *
     * @param xml   String
     * @param clazz class
     * @return JAVABean
     */
    public <T> T toJAVABean(String xml, Class<T> clazz) {
        AssertUtil.notNull(xml);
        AssertUtil.notNull(clazz);
        try {
            return this.readValue(xml, clazz);
        } catch (IOException e) {
            throw new HandlerException(e.getMessage(), e);
        }
    }

    /**
     * XMLString->JAVABeanCollection.
     *
     * @param xml   String
     * @param clazz class...
     * @return Collection
     */
    public <T> Collection<T> toCollection(String xml, Class<? extends Collection<T>> collection, Class<?>... clazz) {
        AssertUtil.notNull(xml);
        AssertUtil.notNull(clazz);
        try {
            return this.readValue(xml, this.getTypeFactory().constructParametricType(collection, clazz));
        } catch (IOException e) {
            throw new HandlerException(e.getMessage(), e);
        }
    }

    /**
     * JAVABean->XMLString.
     *
     * @param javaBean JAVABean
     * @return String
     */
    public String toXMLString(Object javaBean) {
        AssertUtil.notNull(javaBean);
        try {
            return this.writeValueAsString(javaBean);
        } catch (IOException e) {
            throw new HandlerException(e.getMessage(), e);
        }
    }

    /**
     * GET instance.
     *
     * @return XMLMapper
     */
    public static XMLMapper createMobileMapper() {
        return Holder._mobile_instance;
    }

    /**
     * GET instance.
     *
     * @return XMLMapper
     */
    public static XMLMapper createWebMapper() {
        return Holder._web_instance;
    }

    /**
     * static inner class.
     *
     * @author JYD_XL
     */
    private static class Holder {
        /** instance. */
        private static final XMLMapper _mobile_instance = new XMLMapper();

        /** instance. */
        private static final XMLMapper _web_instance = new XMLMapper();

        static {
            _web_instance.setDateFormat(ThreadLocal.withInitial(LocaleDateFormat::new).get()).setTimeZone(TimeZone.getTimeZone(Entity.DEFAULT_TIME_ZONE)).disable(WRITE_NULL_MAP_VALUES).setSerializationInclusion(NON_NULL).disable(FAIL_ON_UNKNOWN_PROPERTIES).enable(SORT_PROPERTIES_ALPHABETICALLY).registerModule(new JaxbAnnotationModule());
        }
    }
}