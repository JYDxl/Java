package org.github.spring.javabean.restful.view;

import org.github.spring.javabean.restful.ReturnType;
import org.github.spring.javabean.restful.VIEWReturn;


import static org.github.spring.javabean.restful.ReturnType.JSP;

/**
 * VIEWJSPReturn.
 *
 * @author JYD_XL
 */
@FunctionalInterface
public interface VIEWJSPReturn extends VIEWReturn {
    @Override
    default ReturnType returntype() {
        return JSP;
    }
}