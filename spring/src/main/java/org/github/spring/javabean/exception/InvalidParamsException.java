package org.github.spring.javabean.exception;

import org.github.spring.footstone.Entity;
import org.github.spring.support.util.StringUtil;

/**
 * InvalidParamsException.
 *
 * @author JYD_XL
 */
@SuppressWarnings("serial")
public class InvalidParamsException extends RunException {

    private String[] params = {};

    /** Constructor. */
    public InvalidParamsException(String... param) {
        params = param;
    }

    @Override
    public String getMessage() {
        return StringUtil.joinWith(Entity.SPACE, "[", params, "]", "以上参数非法.");
    }

    /**
     * Constructor.
     *
     * @param message String
     */
    public InvalidParamsException(String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param cause Throwable
     */
    public InvalidParamsException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message String
     * @param cause   Throwable
     */
    public InvalidParamsException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor.
     *
     * @param message            String
     * @param cause              Throwable
     * @param enableSuppression  boolean
     * @param writableStackTrace boolean
     */
    public InvalidParamsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}