package org.github.spring.javabean.exception;

/**
 * WhatTheFuckException.
 * 
 * @author JYD_XL
 */
@SuppressWarnings("serial")
public class WhatTheFuckException extends RunException {
    /** Constructor. */
    public WhatTheFuckException() {
        super("What the hell are you coding?");
    }
}