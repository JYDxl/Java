package org.github.spring.javabean.restful;

import org.github.spring.support.util.StringUtil;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.Writer;
import java.util.function.Supplier;

/**
 * Top interface of all.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 */
@FunctionalInterface
public interface Returnable extends Serializable, Supplier<String> {
    /**
     * get data by function.
     *
     * @return functional
     */
    default boolean functional() {
        return true;
    }

    /**
     * get resource by loader.
     *
     * @param loader ResourceLoader
     * @return Resource
     */
    default Resource resource(Supplier<ResourceLoader> loader) {
        throw new UnsupportedOperationException(StringUtil.joinWith(SPACE, "GET resource by [", this.getClass().getSimpleName(), "],this function is unsupported."));
    }

    /**
     * get returnType.
     *
     * @return ReturnType
     */
    default ReturnType returntype() {
        return ReturnType.PLAIN;
    }

    /**
     * get terminal.
     *
     * @return terminal
     */
    default boolean terminal() {
        return true;
    }

    /**
     * 通过Writer消费数据.
     *
     * @param writer Writer
     * @throws IOException Exception
     */
    default void accept(Writer writer) throws IOException {
        writer.write(this.get());
    }

    /**
     * 通过OutputStream消费数据.
     *
     * @param outputStream OutputStream
     * @throws IOException Exception
     */
    default void accept(OutputStream outputStream) throws IOException {}

    /**
     * 完成一次请求和响应的全过程，默认处理方法.
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     */
    default void collect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType(this.returntype().get());
        response.setCharacterEncoding(ENCODING);
        if (this.functional()) {
            this.accept(response.getWriter());
        } else {
            this.accept(response.getOutputStream());
        }
    }

    /** default function name of callback. */
    String CALL_BACK = "callback";

    /** empty array. */
    Object[] ARRAY = {};

    /** return code of OK. */
    int RET_OK_CODE = 200;

    /** return message of OK. */
    String RET_OK_MSG = "OK";

    /** return code of ERROR. */
    int RET_ERROR_CODE = 500;

    /** return message of ERROR. */
    String RET_ERROR_MSG = "ERROR";

    /** default count. */
    long COUNT = 0L;

    /** default total. */
    long TOTAL = 0L;

    /** String of space. */
    String SPACE = " ";

    /** String of (. */
    String LEFT_BRACKET = "(";

    /** String of ). */
    String RIGHT_BRACKET = ")";

    /** default encoding of char. */
    String ENCODING = "UTF-8";
}