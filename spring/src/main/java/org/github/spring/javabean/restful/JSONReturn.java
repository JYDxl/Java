package org.github.spring.javabean.restful;

import static org.github.spring.javabean.restful.ReturnType.JSON;

/**
 * JSON返回类型顶层接口.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.restful.Returnable
 * @since 1.0
 */
@FunctionalInterface
public interface JSONReturn extends Returnable {
    @Override
    default ReturnType returntype() {
        return JSON;
    }
}