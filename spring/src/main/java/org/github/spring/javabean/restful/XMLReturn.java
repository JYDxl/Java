package org.github.spring.javabean.restful;

import static org.github.spring.javabean.restful.ReturnType.XML;

/**
 * XMLReturn.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see java.util.function.Supplier
 * @see org.github.spring.javabean.restful.Returnable
 * @since 1.0
 */
@FunctionalInterface
public interface XMLReturn extends Returnable {
    @Override
    default ReturnType returntype() {
        return XML;
    }
}