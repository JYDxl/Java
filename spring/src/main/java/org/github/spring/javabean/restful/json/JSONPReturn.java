package org.github.spring.javabean.restful.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.github.spring.javabean.restful.JSONReturn;
import org.github.spring.javabean.restful.ReturnType;
import org.github.spring.support.util.StringUtil;

import static org.github.spring.javabean.restful.ReturnType.JSONP;
import static org.github.spring.javabean.restful.Returnable.CALL_BACK;

/**
 * JSONPReturn.
 *
 * @param <T> data
 * @author JYD_XL
 */
@SuppressWarnings("serial")
@JsonIgnoreProperties(CALL_BACK)
public class JSONPReturn<T> extends JSONDataReturn<T> implements JSONReturn {
    /** callback. */
    private String callback = CALL_BACK;

    /**
     * GET callback.
     *
     * @return String
     */
    public String getCallback() {
        return callback;
    }

    /**
     * SET callback.
     *
     * @param callback String
     * @return JSONPReturn
     */
    public JSONPReturn<T> setCallback(String callback) {
        if (StringUtil.isNotBlank(callback)) { this.callback = callback;}
        return this;
    }

    /** Constructor. */
    public JSONPReturn() {}

    /**
     * Constructor.
     *
     * @param callback String
     * @param data     T
     */
    public JSONPReturn(String callback, T data) {
        this.setCallback(callback).setData(data);
    }

    @Override
    public boolean functional() {
        return true;
    }

    @Override
    public String get() {
        return StringUtil.join(callback, SPACE, LEFT_BRACKET, super.get(), RIGHT_BRACKET);
    }

    @Override
    public ReturnType returntype() {
        return JSONP;
    }

    @Override
    public String toString() {
        return super.get();
    }
}