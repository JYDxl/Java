package org.github.spring.javabean.restful.xml;

import java.io.IOException;
import java.io.OutputStream;
import org.github.spring.javabean.restful.XMLMapper;
import org.github.spring.javabean.restful.XMLReturn;

/**
 * XMLResponse.
 *
 * @param <T> type
 * @author JYD_XL
 */
@SuppressWarnings("serial")
public class XMLResponse<T> implements XMLReturn {
    /** data. */
    private T data;

    /**
     * GET data.
     *
     * @return data
     */
    public T getData() {
        return data;
    }

    /**
     * SET data.
     *
     * @param data T
     */
    public void setData(T data) {
        this.data = data;
    }

    /** Constructor. */
    public XMLResponse() {}

    /**
     * Constructor.
     *
     * @param data T
     */
    public XMLResponse(T data) {
        this.data = data;
    }

    @Override
    public boolean functional() {
        return false;
    }

    @Override
    public String get() {
        return this.createMapper().toXMLString(this);
    }

    @Override
    public void accept(OutputStream outputStream) throws IOException {
        this.createMapper().writeValue(outputStream, this);
    }

    private XMLMapper createMapper() {
        return XMLMapper.createWebMapper();
    }
}