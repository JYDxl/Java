package org.github.spring.footstone;

import java.io.Serializable;
import org.apache.ibatis.session.RowBounds;
import org.github.spring.support.util.StringUtil;
import org.github.spring.system.component.PageStarter;

/**
 * Abstract query conditional data model,inherit this class when executing a paging query.
 *
 * @author JYD_XL
 * @version 1.0
 */
@SuppressWarnings("serial")
public abstract class AbstractCondModel implements Serializable {
    /** sort_name. */
    private String sortName;

    /** sort_order. */
    private String sortOrder;

    /** page_size. */
    private int pageSize = 10;

    /** page_number. */
    private int pageNumber = 1;

    /** page_flag. */
    private boolean pageFlag = true;

    /**
     * GET pageSize.
     *
     * @return integer
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * SET pageSize.
     *
     * @param pageSize integer
     * @return AbstractCondModel
     */
    public AbstractCondModel setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    /**
     * GET pageNumber.
     *
     * @return integer
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * SET pageNumber.
     *
     * @param pageNumber integer
     * @return AbstractCondModel
     */
    public AbstractCondModel setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
        return this;
    }

    /**
     * GET sortName.
     *
     * @return String
     */
    public String getSortName() {
        return sortName;
    }

    /**
     * SET sortName.
     *
     * @param sortName String
     * @return AbstractCondModel
     */
    public AbstractCondModel setSortName(String sortName) {
        if (sortName.matches(SORT_NAME_PATTERN)) {this.sortName = sortName;}
        return this;
    }

    /**
     * GET sortOrder.
     *
     * @return String
     */
    public String getSortOrder() {
        return sortOrder;
    }

    /**
     * SET sortOrder.
     *
     * @param sortOrder String
     * @return AbstractCondModel
     */
    public AbstractCondModel setSortOrder(String sortOrder) {
        if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {this.sortOrder = sortOrder.toUpperCase();}
        return this;
    }

    /**
     * GET pageFlag.
     *
     * @return boolean
     */
    public boolean getPageFlag() {
        return pageFlag;
    }

    /**
     * SET pageFlag.
     *
     * @param pageFlag boolean
     * @return AbstractCondModel
     */
    public AbstractCondModel setPageFlag(boolean pageFlag) {
        this.pageFlag = pageFlag;
        return this;
    }

    /**
     * Get sort_info,this method must be covered in use handwritten SQL.
     *
     * @return sort_info
     */
    public String getSortInfo() {
        if (StringUtil.isBlank(sortName) || StringUtil.isBlank(sortOrder)) {return null;}
        StringBuilder column = new StringBuilder();
        for (int i = 0; i < sortName.length(); i++) {
            if (Character.isUpperCase(sortName.charAt(i))) {
                column.append("_").append(Character.toLowerCase(sortName.charAt(i)));
            } else {
                column.append(sortName.charAt(i));
            }
        }
        return column.append(" ").append(sortOrder).toString();
    }

    /**
     * Assert the validity of the params.
     *
     * @return AbstractCondModel
     */
    public AbstractCondModel assertParams() {
        return this;
    }

    /**
     * Perform paging.
     *
     * @return AbstractCondModel
     */
    public AbstractCondModel paging() {
        PageStarter.startPage(this);
        return this;
    }

    /**
     * Start method injection.
     *
     * @return AbstractCondModel
     */
    public AbstractCondModel invokeSetMethod() {
        return this;
    }

    /**
     * Get RowBounds.
     *
     * @return RowBounds
     */
    public RowBounds getRowBounds() {
        return new RowBounds(pageNumber, this.getDataSize());
    }

    /** Get data size,part or full. */
    public int getDataSize() {
        return pageFlag ? pageSize : 0;
    }

    /** pattern of sort name. */
    private static final String SORT_NAME_PATTERN = "^[A-Za-z_$]+[A-Za-z_$\\\\d]+$";
}