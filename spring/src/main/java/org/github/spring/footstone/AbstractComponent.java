package org.github.spring.footstone;

import org.springframework.beans.factory.BeanNameAware;

/**
 * 抽象组件基类，提供BeanName设置以及log信息.
 *
 * @author JYD_XL
 * @version 1.0
 * @see java.io.Serializable
 * @see org.github.spring.footstone.Entity
 * @see org.springframework.beans.factory.Aware
 * @see org.springframework.beans.factory.BeanNameAware
 */
public abstract class AbstractComponent extends Entity implements BeanNameAware {
    /** beanName. */
    private String id;

    @Override
    public void setBeanName(String beanName) {
        id = beanName;
    }

    /**
     * GET beanName.
     *
     * @return String
     */
    public String getBeanName() {
        return id;
    }
}