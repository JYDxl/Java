package org.github.spring.footstone;

/**
 * 常量接口，存储Bean的名称集合.
 *
 * @author JYD_XL
 * @version 1.0
 */
public interface Interface {
    String BEAN_PROPERTIES_HOLDER = "propsHolder";

    String BEAN_PAGE_STARTER = "pageStarter";

    String BEAN_MYBATIS_DATASOURCE = "myBatisDataSource";

    String BEAN_LOCAL_SESSION_FACTORY = "localSessionFactory";

    String BEAN_HIBERNATE_DATASOURCE = "hibernateDataSource";

    String BEAN_SQL_SESSION_FACTORY = "sqlSessionFactoryBean";
}