package org.github.spring.footstone;

import java.io.Serializable;

/**
 * 常量抽象类，存储默认的属性集合.
 * <p>
 * Created by JYD_XL on 2017/5/7.
 */
@SuppressWarnings("serial")
public abstract class Entity implements Serializable {
    /** 时区. */
    public static final String DEFAULT_TIME_ZONE = "GMT+08:00";

    /** 日期格式化形式. */
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /** 编码形式. */
    public static final String DEFAULT_ENCODING = "utf-8";

    /** 配置文件路径. */
    public static final String DEFAULT_PROFILE_LOCATION = "classpath:props/profile.properties";

    /** DRUID数据源DriverClassFullName. */
    public static final String DRUID_DRIVER_CLASS_NAME = "druid.driverClassName";

    public static final String QUERY = "query";

    public static final String PROJECT_ROOT = "root";

    public static final String HIKARI_DRIVER_CLASS_NAME = "hikari.driverClassName";

    public static final String HIKARI_JDBC_URL = "hikari.jdbcUrl";

    public static final String HIKARI_USERNAME = "hikari.username";

    public static final String HIKARI_PASSWORD = "hikari.password";

    public static final String HIKARI_MAXIMUM_POOL_SIZE = "hikari.maximumPoolSize";

    public static final String HIKARI_MINIMUM_IDLE = "";

    public static final String HIKARI_IDLE_TIMEOUT = "hikari.idleTimeout";

    public static final String HIKARI_MAX_LIFETIME = "";

    public static final String MYBATIS_CONFIG_LOCATION = "classpath:appctx/ApplicationContext-myBatis.xml";

    public static final String MYBATIS_MAPPER_LOCATIONS = "classpath*:sqlmapper/**/*.xml";

    public static final String EMPTY = "";

    public static final String EMPTY_STRING_JSON = "{}";

    public static final String ASC = "ASC";

    public static final String DESC = "DESC";

    public static final int PAGE_SIZE = 10;

    public static final int PAGE_NUMBER = 1;

    public static final boolean PAGING = true;

    public static final String SPACE = " ";

    public static final String SORT_NAME="sortName";

    public static final String SORT_ORDER = "sortOrder";
}